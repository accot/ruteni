from http import HTTPStatus
from typing import Optional

from ruteni.core.types import HTTPApp
from ruteni.responses.errors import HTTP_RESPONSE
from ruteni.responses.json import JSONResponse


class HTTPException(Exception):
    def __init__(self, status: HTTPStatus, phrase: str = None) -> None:
        Exception.__init__(self, phrase or status.phrase)
        self.status = status

    def __repr__(self) -> str:
        class_name = self.__class__.__name__
        return (
            f"{class_name}(status={self.status.value!r}, phrase={self.status.phrase!r})"
        )

    @property
    def response(self) -> HTTPApp:
        return HTTP_RESPONSE[self.status]


def mkexc(status: HTTPStatus) -> type[Exception]:
    class HTTPException(Exception):
        response = HTTP_RESPONSE[status]

        def __init__(self) -> None:
            Exception.__init__(self, f"HTTP error: {status.phrase}")

    return HTTPException


Http400BadRequestException = mkexc(HTTPStatus.BAD_REQUEST)
Http401UnauthorizedException = mkexc(HTTPStatus.UNAUTHORIZED)
Http403ForbiddenException = mkexc(HTTPStatus.FORBIDDEN)
Http404NotFoundException = mkexc(HTTPStatus.NOT_FOUND)
Http405MethodNotAllowedException = mkexc(HTTPStatus.METHOD_NOT_ALLOWED)
Http415UnsupportedMediaTypeException = mkexc(HTTPStatus.UNSUPPORTED_MEDIA_TYPE)
Http422UnprocessableEntityException = mkexc(HTTPStatus.UNPROCESSABLE_ENTITY)
Http500InternalServerErrorException = mkexc(HTTPStatus.INTERNAL_SERVER_ERROR)


class RuteniHTTPException(HTTPException):
    def __init__(
        self,
        service: str,
        error: str,
        *,
        status: HTTPStatus = HTTPStatus.BAD_REQUEST,
        public: Optional[dict] = None,
        private: Optional[dict] = None,
    ) -> None:
        super().__init__(status, f"{service}: {error}")
        self.path = [service]
        self.error = error
        self.status = status
        self.public = public
        self.private = private

    def push_path(self, service: str) -> None:
        self.path.append(service)

    def __repr__(self) -> str:
        class_name = self.__class__.__name__
        # TODO: safe to include private? could it be sent to the client by mistake?
        return (
            f"{class_name}(status={self.status.value!r}, path={self.path!r}, "
            "error={self.error!r}, public={self.public})"
        )

    @property
    def response(self) -> HTTPApp:
        # TODO: log private data
        report = {"service": self.path[0], "error": self.error, "data": self.public}
        return JSONResponse(report, status=self.status.value)
