import logging
from contextlib import _AsyncGeneratorContextManager
from typing import Optional

import asyncpg
from asyncpg.connection import Connection
from asyncpg.exceptions import PostgresLogMessage
from databases import DatabaseURL
from sqlalchemy.ext.asyncio import AsyncConnection, AsyncEngine, create_async_engine

from ruteni.config import config
from ruteni.services import Service
from ruteni.state import State
from ruteni.utils.database import AsyncLoggerContextManager, PostgresLogger

logger = logging.getLogger(__name__)

DATABASE_URL: DatabaseURL = config.get("RUTENI_DATABASE_URL", cast=DatabaseURL)


class DatabaseService(Service):
    def __init__(self, database_url: DatabaseURL) -> None:
        Service.__init__(self, "database", 1)
        self.engine: Optional[AsyncEngine] = None
        self.database_url = database_url

    async def startup(self, state: State) -> None:
        await super().startup(state)
        self.engine = create_async_engine(str(self.database_url))

    async def shutdown(self, state: State) -> None:
        await super().shutdown(state)
        if self.engine:
            await self.engine.dispose()
            self.engine = None

    def connect(self) -> AsyncConnection:
        assert self.engine
        return self.engine.connect()

    def begin(self) -> _AsyncGeneratorContextManager[AsyncConnection]:
        # ->  AsyncContextManager[AsyncGenerator[AsyncConnection, None]]:
        assert self.engine
        return self.engine.begin()

    def log(self, conn: Connection, log: PostgresLogger) -> AsyncLoggerContextManager:
        assert self.engine
        return AsyncLoggerContextManager(self.engine, conn, log)


database = DatabaseService(DATABASE_URL)
