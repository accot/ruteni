import logging

from ruteni.state import State

from ruteni.models import metadata
from ruteni.services import Service
from ruteni.services.database import database

logger = logging.getLogger(__name__)


class ModelsService(Service):
    def __init__(self) -> None:
        Service.__init__(self, "models", 1, {database})

    async def startup(self, state: State) -> None:
        await super().startup(state)
        async with database.begin() as conn:
            await conn.run_sync(metadata.create_all)

    async def shutdown(self, state: State) -> None:
        await super().shutdown(state)


models = ModelsService()
