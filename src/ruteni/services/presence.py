import logging

from socketio import AsyncNamespace
from socketio.exceptions import ConnectionRefusedError
from sqlalchemy import and_, func
from ruteni.state import State

from ruteni.config import config
from ruteni.models.connections import connections
from ruteni.plugins.sessions import Session
from ruteni.plugins.socketio import sio
from ruteni.plugins.identity import _dbr_get_user_from_environ
from ruteni.services import Service
from ruteni.services.database import database
from ruteni.services.keys import key_service
from ruteni.services.server import server

logger = logging.getLogger(__name__)

NAMESPACE = config.get("RUTENI_PRESENCE_NAMESPACE", default="/ruteni/presence")


class PresenceNamespace(AsyncNamespace):
    async def on_connect(self, sid: str, environ: dict[str, str]) -> bool:
        # async with sio.eio.session(sid) as session:
        #     session["username"] = username

        # get the current user from the HTTP_COOKIE cookie
        async with database.connect() as conn:
            user = await _dbr_get_user_from_environ(conn, environ)
            if user is None:
                raise ConnectionRefusedError("not authenticated")

            connection_id = await conn.scalar(
                connections.insert()
                .values(
                    sid=sid,
                    user_id=user.id,
                    ip_address=environ["REMOTE_ADDR"],
                    server_id=server.id,
                )
                .returning(connections.c.id)
            )
        token = key_service.encrypt(str(connection_id))
        await self.emit("token", token)
        logger.info("%s is connected", user.display_name)
        return True

    async def on_disconnect(self, sid: str) -> None:
        # TODO: could there be identical sids for different connections over time?
        async with database.begin() as conn:
            await conn.execute(
                connections.update()
                .where(
                    and_(connections.c.sid == sid, connections.c.closed_at.is_(None))
                )
                .values(closed_at=func.now())
            )
        logger.info("%s.disconnect", sid)


class PresenceService(Service):
    def __init__(self) -> None:
        Service.__init__(self, "presence", 1, {key_service, server})

    async def startup(self, state: State) -> None:
        await super().startup(state)
        sio.register_namespace(PresenceNamespace(NAMESPACE))

    async def shutdown(self, state: State) -> None:
        await super().shutdown(state)
        # TODO: unregister


presence = PresenceService()
