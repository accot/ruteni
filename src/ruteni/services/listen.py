import json
import logging
from collections.abc import Awaitable, Callable
from typing import Any, Optional, cast

from asyncpg.connection import Connection
from ruteni.state import State

from ruteni.services import Service
from ruteni.services.database import database
from ruteni.services.server import server

logger = logging.getLogger(__name__)

Listener = Callable[[Connection, int, str, Any], Awaitable[None]]


class ListenService(Service):
    def __init__(self) -> None:
        Service.__init__(self, "listen", 1, {database, server})
        self.channel: Optional[str] = None
        self.listeners: dict[str, list[Listener]] = {}

    async def startup(self, state: State) -> None:
        await super().startup(state)
        channel = f"RUTENI_{server.id}"
        logger.debug("LISTENING %s", channel)
        async with database.connect() as conn:
            raw_connection = await conn.get_raw_connection()
            driver_connection = cast(Connection, raw_connection.driver_connection)
            # TODO: manager other dialects/drivers
            await driver_connection.add_listener(channel, self.callback)
            self.channel = channel

    async def shutdown(self, state: State) -> None:
        await super().shutdown(state)
        if self.channel:
            async with database.connect() as conn:
                raw_connection = await conn.get_raw_connection()
                driver_connection = cast(Connection, raw_connection.driver_connection)
                # TODO: the doc suggests a listener is linked to a specific connection
                await driver_connection.remove_listener(self.channel, self.callback)
                self.channel = None

    async def callback(
        self, connection: Connection, pid: int, channel: str, payload: str
    ) -> None:
        logger.debug("NOTIFICATION %s", payload)
        subject, data = payload.split(" ", 1)
        if subject not in self.listeners:
            logger.warning("no listener for %s", subject)
            return
        for listener in self.listeners[subject]:
            await listener(connection, pid, subject, data)  # TODO: allow sync

    def add_listener(self, subject: str, listener: Listener) -> None:
        if subject not in self.listeners:
            self.listeners[subject] = []
        self.listeners[subject].append(listener)

    def remove_listener(self, subject: str, listener: Listener) -> None:
        assert subject in self.listeners, f"not listener for {subject}"
        self.listeners[subject].remove(listener)


listen_service = ListenService()
