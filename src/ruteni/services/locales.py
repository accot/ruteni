from collections.abc import Mapping, Sequence
from typing import Optional

from babel.core import Locale, get_locale_identifier
from bidict import bidict
from sqlalchemy import and_
from sqlalchemy.ext.asyncio import AsyncConnection
from sqlalchemy.sql import select
from ruteni.state import State

from ruteni.models.locales import locales
from ruteni.services import Service
from ruteni.services.database import database
from ruteni.services.models import models


async def _dbr_get_locale_id(
    conn: AsyncConnection, locale: str, sep: str = "_"
) -> Optional[int]:
    locale_ = Locale.parse(locale, sep=sep)
    async with database.connect() as conn:
        return await conn.scalar(
            select(locales.c.id).where(
                and_(
                    locales.c.language == locale_.language,
                    locales.c.territory == locale_.territory,
                    locales.c.disabled_at.is_(None),
                )
            )
        )


class LocalesService(Service):
    def __init__(self) -> None:
        Service.__init__(self, "locales", 1, {models})
        self.locales: Optional[Mapping[Locale, int]] = None

    async def startup(self, state: State) -> None:
        await super().startup(state)
        async with database.connect() as conn:
            result = await conn.execute(
                select(locales.c.id, locales.c.language, locales.c.territory).where(
                    locales.c.disabled_at.is_(None)
                )
            )
            self.locales = bidict(
                {
                    Locale(language, territory): id
                    for id, language, territory in result.fetchall()
                }
            )

    async def shutdown(self, state: State) -> None:
        await super().shutdown(state)
        self.locales = None

    def negotiate(self, preferred: Sequence[str]) -> Locale:
        assert self.locales, "Empty locales; service started?"
        available = tuple(str(locale) for locale in self.locales.keys())  # TODO: cache?
        negotiated = Locale.negotiate(preferred, available)
        return negotiated or Locale("en", "US")

    def get_locale_id(self, locale: str, sep: str = "_") -> Optional[int]:
        assert self.locales, "Empty locales; service started?"
        return self.locales.get(Locale.parse(locale, sep=sep), None)

    @staticmethod
    def get_html_lang(locale: Locale) -> str:
        return get_locale_identifier(
            (locale.language, locale.territory, locale.script, locale.variant), sep="-"
        )


locales_service = LocalesService()
