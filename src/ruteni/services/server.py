import logging

import ifaddr
from sqlalchemy import func
from sqlalchemy.sql import select
from ruteni.state import State

from ruteni.models.servers import servers
from ruteni.services import Service
from ruteni.services.database import database
from ruteni.services.models import models

logger = logging.getLogger(__name__)


class ServerService(Service):
    def __init__(self) -> None:
        Service.__init__(self, "server", 1, {models})
        self.id: int = None

    async def startup(self, state: State) -> None:
        await super().startup(state)
        adapters = ifaddr.get_adapters()
        # TODO: which address to select?
        for adapter in adapters:
            for ip in adapter.ips:
                ip_address = ip.ip
                break
        async with database.begin() as conn:
            self.id = await conn.scalar(
                servers.insert().values(ip_address=ip_address).returning(servers.c.id)
            )

    async def shutdown(self, state: State) -> None:
        await super().shutdown(state)
        async with database.begin() as conn:
            stopped = await conn.scalar(
                select(servers.c.stopped_at.isnot(None)).where(servers.c.id == self.id)
            )
            if stopped:
                logger.warning("server already stopped in database!")
                return
            await conn.execute(
                servers.update()
                .where(servers.c.id == self.id)
                .values(stopped_at=func.now())
            )
        self.id = None


server = ServerService()
