import asyncio
import logging

import aionotify
from ruteni.state import State

from ruteni.services import Service

# TODO: test other libraries

logger = logging.getLogger(__name__)


class AionotifyService(Service):
    def __init__(self) -> None:
        Service.__init__(self, "aionotify", 1)
        self.watcher = aionotify.Watcher()

    def watch(self, path: str, *, alias: str = None) -> None:
        self.watcher.watch(alias=alias, path=path, flags=aionotify.Flags.MODIFY)

    async def work(self) -> None:
        # TODO: implement
        loop = asyncio.get_event_loop()
        await self.watcher.setup(loop)
        for _i in range(10):
            event = await self.watcher.get_event()
            print(event)
        self.watcher.close()

    async def startup(self, state: State) -> None:
        await Service.startup(self, state)
        # TODO: create task

    async def shutdown(self, state: State) -> None:
        await Service.shutdown(self, state)
        # TODO: stop task


inotify_service = AionotifyService()
