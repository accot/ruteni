import importlib
import logging
import pkgutil

logger = logging.getLogger(__name__)


def load_all_submodules(modname: str, path: list[str]) -> None:
    for finder, name, ispkg in pkgutil.iter_modules(path):
        importlib.import_module(f"{modname}.{name}")
        logger.debug(f"loaded {modname}.{name}")
