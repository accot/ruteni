import asyncio
from collections.abc import Iterable
from typing import Optional, Tuple

from asgiref.typing import HTTPScope

from ruteni.routing.types import Header, HeadersProvider

CONTENT_DISPOSITION_HEADER_KEY = b"content-disposition"
CONTENT_LENGTH_HEADER_KEY = b"content-length"
CONTENT_TYPE_HEADER_KEY = b"content-type"
COOKIE_HEADER_KEY = b"cookie"
ETAG_HEADER_KEY = b"etag"
LAST_MODIFIED_HEADER_KEY = b"last-modified"
LOCATION_HEADER_KEY = b"location"
USER_AGENT_HEADER_KEY = b"user-agent"


def find_header(headers: Iterable[Header], name: bytes) -> Optional[bytes]:
    for key, val in headers:
        if key == name:
            return val
    return None


def get_header(scope: HTTPScope, name: bytes) -> Optional[bytes]:
    return find_header(scope["headers"], name)


def get_content_type_and_auth_headers(
    scope: HTTPScope, auth_header_name: bytes = COOKIE_HEADER_KEY
) -> tuple[Optional[bytes], Optional[bytes]]:
    content_type_header = None
    auth_header = None
    for key, val in scope["headers"]:
        if key == CONTENT_TYPE_HEADER_KEY:
            content_type_header = val
            if auth_header is not None:
                break
        elif key == auth_header_name:
            auth_header = val
            if content_type_header is not None:
                break
    return content_type_header, auth_header


def search_headers(
    headers: Iterable[Header], keys: Iterable[bytes]
) -> list[Optional[bytes]]:
    # TODO: benchmark this against a header as dict
    vals: list[Optional[bytes]] = []
    for k2 in keys:
        for k1, val in headers:
            if k1 == k2:
                # TODO: manage multiple occurrences of the same key
                vals.append(val)
                break
        else:
            vals.append(None)
    return vals


def get_user_agent(scope: HTTPScope) -> Optional[bytes]:
    return find_header(scope["headers"], USER_AGENT_HEADER_KEY)


class HeadersGatherer:
    def __init__(self, *providers: * Tuple[HeadersProvider]) -> None:
        self.providers = providers

    async def __call__(self, scope: HTTPScope) -> Tuple[Header, ...]:
        tasks_generator = (provider(scope) for provider in self.providers)
        headers_tuples = await asyncio.gather(*tasks_generator)
        return sum(headers_tuples, ())  # concatenates headers
