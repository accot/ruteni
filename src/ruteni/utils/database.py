from collections.abc import Callable

from asyncpg.connection import Connection
from asyncpg.exceptions import PostgresLogMessage
from sqlalchemy.ext.asyncio import AsyncConnection, AsyncEngine


async def get_driver_connection(
    engine: AsyncEngine, conn: AsyncConnection
) -> Connection:  # asyncpg.connection:
    raw_connection = await conn.get_raw_connection()
    dbapi_connection = raw_connection.dbapi_connection
    assert dbapi_connection
    # return dbapi_connection.driver_connection
    return engine.dialect.get_driver_connection(dbapi_connection)


PostgresLogger = Callable[[Connection, PostgresLogMessage], None]


class AsyncLoggerContextManager:
    def __init__(
        self, engine: AsyncEngine, conn: AsyncConnection, logger: PostgresLogger
    ) -> None:
        self.engine = engine  # TODO: from conn?
        self.conn = conn
        self.logger = logger

    async def __aenter__(self):
        asyncpg_connection = await get_driver_connection(self.engine, self.conn)
        asyncpg_connection.add_log_listener(self.logger)

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        asyncpg_connection = await get_driver_connection(self.engine, self.conn)
        asyncpg_connection.remove_log_listener(self.logger)
