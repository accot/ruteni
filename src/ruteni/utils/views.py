from sqlalchemy import MetaData, event
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.schema import DDLElement
from sqlalchemy.sql import table
from sqlalchemy.sql.compiler import DDLCompiler
from sqlalchemy.sql.selectable import Selectable

# inspired by https://github.com/sqlalchemy/sqlalchemy/wiki/Views
# TODO: use create_view (https://sqlalchemy-utils.readthedocs.io/en/latest/view.html)
# when this is fixed: https://github.com/kvesteri/sqlalchemy-utils/issues/441


class ViewDDLElement(DDLElement):
    def __init__(self, name: str, schema: str = None) -> None:
        self.name = f"{schema}.{name}" if schema else name


class CreateView(ViewDDLElement):
    def __init__(self, name: str, selectable: Selectable, schema: str = None) -> None:
        super().__init__(name, schema)
        self.selectable = selectable


class DropView(ViewDDLElement):
    pass


@compiles(CreateView)
def compile_create_view(element: DDLElement, compiler: DDLCompiler) -> str:
    code = compiler.sql_compiler.process(element.selectable, literal_binds=True)
    return "CREATE VIEW %s AS %s" % (element.name, code)


@compiles(DropView)
def compile_drop_view(element: DDLElement, compiler: DDLCompiler) -> str:
    return "DROP VIEW %s" % element.name


def view(name: str, metadata: MetaData, selectable: Selectable) -> table:
    t = table(name)

    for c in selectable.c:
        c._make_proxy(t)

    schema = metadata.schema
    event.listen(metadata, "after_create", CreateView(name, selectable, schema))
    event.listen(metadata, "before_drop", DropView(name, schema))
    return t
