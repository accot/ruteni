import logging from "external:@totates/logging/v1";
import blaze from "external:@totates/blaze-css/v10";
import { getLanguage, registerElementBundleResolver } from "external:@totates/translation/v1";
import policy from "external:@totates/trusted-types/v1";
import { QuotquotElement } from "external:@totates/quotquot/v1:element";
import EventsRenderer from "external:@totates/quotquot/v1:plugin.events";
import FluentRenderer from "external:@totates/quotquot/v1:plugin.fluent";
import StylePlugin from "external:@totates/quotquot/v1:plugin.style";
import ClassRenderer from "external:@totates/quotquot/v1:plugin.class";
// import TemplateRenderer from "external:@totates/quotquot/v1:plugin.template";
import { io } from "external:@totates/socket.io/v4";

import html from "./index.html";
import index from "./index.json";
import resources from "./resources.js";

const logger = logging.getLogger("ruteni.plugin.registration.element.form");
// logger.setLevel("WARN");

registerElementBundleResolver(index, import.meta.url);

class TimeoutError extends Error {
    constructor(args, timeout) {
        super(`${args[0]} timed out after ${timeout} milliseconds`);
        this.args = args;
        this.timeout = timeout;
    }
}

const template = document.createElement("template");
template.innerHTML = policy.createHTML(html);

// TODO: use logo custom element from site component

class RegistrationFormHTMLElement extends QuotquotElement {
    constructor() {
        const state = {
            minutes: -1,
            remaining: -1,
            issues: {},
            state: "editing",
            styles: blaze.urlsFromManifest(index),
            events: {
                "submit-form": e => {
                    e.preventDefault();
                    logger.debug("submit-form");
                    this.submit().catch(logger.error);
                },
                "verify-code": e => {
                    e.preventDefault();
                    this.verify().catch(logger.error);
                },
                "back-home": e => this.update({ state: "editing" }).catch(logger.error)
            }
        };
        super({
            template,
            state,
            Renderers: [ClassRenderer, EventsRenderer, FluentRenderer/*, TemplateRenderer*/],
            Plugins: [StylePlugin]
        });
        this._socket = io("/ruteni/registration", { transports: ["websocket"] });
        this._socket.on("connect", () => logger.debug("connected"));
        this._socket.on("disconnect", reason => logger.debug("disconnected", reason));
        this._socket.on("connect_error", logger.error);
    }

    /*
    connectedCallback() {
        super.connectedCallback();
        connectRoot(componentName, this.shadowRoot);
        translateFragment(componentName, this.shadowRoot).catch(logger.error);
        logger.debug(`added ${index.tagName} to ${componentName} localization`);
    }

    disconnectedCallback() {
        super.connectedCallback();
        disconnectRoot(componentName, this.shadowRoot);
        logger.debug(`removed ${index.tagName} from ${componentName} localization`);
    }
    */

    call(timeout, ...args) {
        let timeoutId;
        return Promise.race([
            new Promise(resolve => this._socket.emit(...args, result => {
                clearTimeout(timeoutId); // TODO: race condition on timeoutId?
                resolve(result);
            })),
            new Promise((resolve, reject) => {
                timeoutId = setTimeout(() => {
                    const error = new TimeoutError(args, timeout);
                    reject(error);
                }, timeout);
            })
        ]);
    }

    async submit() {
        // logger.debug(displayName.validity);
        // const form = this.shadowRoot.forms.register;
        const form = this.$('form[name="register"]');
        const { displayName, email, password, passwordCheck } = form.elements;
        try {
            var issues = await this.call(5000, "edit", { // eslint-disable-line no-var
                display_name: displayName.value, // eslint-disable-line camelcase
                email: email.value,
                password: password.value,
                locale: getLanguage()
            });
        }
        catch (exc) {
            if (exc instanceof TimeoutError)
                await this.update({ issues: { "server-timeout": true } });
            logger.error(exc);
            return;
        }

        if (password.value !== passwordCheck.value)
            issues["password-mismatch"] = true;

        if (Object.keys(issues).length)
            await this.update({ issues });
        else
            await this.register();
    }

    async register() {
        try {
            var minutes = await this.call(5000, "register"); // eslint-disable-line no-var
        }
        catch (exc) {
            if (exc instanceof TimeoutError)
                await this.update({ issues: { "server-timeout": true } });
            logger.error(exc);
            return;
        }

        if (minutes)
            await this.update({ state: "delivered", minutes });
        else
            await this.update({ state: "delivery-failed" });
    }

    async verify() {
        const form = this.$('form[name="verify"]');
        const code = form.elements.verificationCode.value;
        form.elements.verificationCode.value = "";
        try {
            var result = await this.call(5000, "verify", code); // eslint-disable-line no-var
        }
        catch (exc) {
            if (exc instanceof TimeoutError)
                await this.update({ issues: { "server-timeout": true } });
            logger.error(exc);
            return;
        }

        // TODO: manage {error: "invalid-arguments"}
        if (result === null) {
            this._socket.disconnect();
            await this.update({ state: "confirmed" });
            const event = new window.CustomEvent("registered", { detail: null });
            this.dispatchEvent(event);
        }
        else if (result === -1)
            await this.update({ state: "code-expired" });
        else if (result === 0)
            await this.update({ state: "too-many-attempts" });
        else if (result > 0)
            await this.update({ remaining: result });
        else
            logger.error(result); // TODO: notify user
    }

}

window.customElements.define(index.tagName, RegistrationFormHTMLElement);
