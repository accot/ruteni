already-have-an-account = Already have an account?

at-least-eight-characters =
    .placeholder = At least 8 characters

back-home = Go back to registration form

check-your-mailbox = We sent a message to your email address with a verification code. Please read this message and copy the verification code below within { $minutes } minutes.

code-expired = The verification code expired. Please try to register again.

create-account = Create account

create-your-account =
    .value = Create your account

delivered = Check your mailbox…

delivery-failed = We failed to deliver a confirmation message to your email address.

email = Email

email-confirmed = You successfully confirmed your email address. Your account is now active.

empty-display-name = Your name cannot be empty

empty-email = Your email cannot be empty

empty-locale = Your locale cannot be empty

empty-password = Your password cannot be empty

enter-code = Enter the verification code that was sent to you by email

enter-valid-email = Enter a valid email address

enter-your-name = Enter your name

how-to-enable-cookies = How to enable cookies

incomplete-email = Your email address is incomplete

misconfigured-domain-email = Your email domain is misconfigured

overflow-display-name = Your name is too long

overflow-email = Your email is too long

overflow-password = Your password is too long

parse-error-email = Your email address has a wrong format

password = Password

password-eight-characters = Passwords must be at least 8 characters.

password-mismatch = Passwords do not match

password-must-match = Passwords must match

please-enable-cookies = Please Enable Cookies to Continue

reenter-password = Re-enter password

server-timeout = The server did not reply. Please retry.

signin = Sign-In

too-many-attempts = You made too many errors. Please try to register again.

unknown-domain-email = The domain of this email address is unknown

unknown-locale = This locale is unknown

verify-email =
    .value = Verify

weak-password = This password is too weak

wrong-code-length = Wrong code length

wrong-code-value = The code is incorrect. { $remaining } attempts remaining.

your-name = Your name
