import { Component } from "totates";
import data from "./component.json" assert { type: "json" };
const component = new Component(data, { installDir: "../../../dist" });
export default component.getConfig();
