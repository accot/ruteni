import logging from "external:@totates/logging/v1";
import "external:@totates/quotquot/v1:router";
import "external:@totates/pwa/v1";
import "external:@ruteni/store/v1:element.store-homepage.index";
import "external:@ruteni/store/v1:element.store-settings.index";
import "external:@totates/quotquot/v1:element.unknown-page.index";

import routes from "./routes.json";

const logger = logging.getLogger("ruteni.app.store");
logger.setLevel("DEBUG");


async function main() {

    const hashRouter = document.createElement("hash-router");

    for (const [elementName, route] of routes) {
        const child = document.createElement(route.type);
        child.setAttribute("element", elementName);
        child.setAttribute("path", route.path);
        if (route.keep)
            child.setAttribute("keep", "");
        hashRouter.appendChild(child);
    }

    const unknownPage = document.createElement("unknown-page");
    const defaultRoute = document.createElement("default-route");
    defaultRoute.appendChild(unknownPage);
    hashRouter.appendChild(defaultRoute);

    document.body.appendChild(hashRouter);
    logger.debug("configured");
}

main().catch(logger.error);

/*
  to trigger a csp report:
const script = document.createElement("script");
script.textContent = "console.log("salut")";
document.body.appendChild(script);
*/
