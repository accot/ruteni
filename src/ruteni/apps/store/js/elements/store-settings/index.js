import blaze from "external:@totates/blaze-css/v10";
import policy from "external:@totates/trusted-types/v1";
import { registerElementBundleResolver } from "external:@totates/translation/v1";
import { QuotquotElement } from "external:@totates/quotquot/v1:element";
import EventsRenderer from "external:@totates/quotquot/v1:plugin.events";
import FluentRenderer from "external:@totates/quotquot/v1:plugin.fluent";
import StylePlugin from "external:@totates/quotquot/v1:plugin.style";
import ClassRenderer from "external:@totates/quotquot/v1:plugin.class";

import html from "./index.html";
import index from "./index.json";
import resources from "./resources.js";

registerElementBundleResolver(index, import.meta.url);

const template = document.createElement("template");
template.innerHTML = policy.createHTML(html);

class StoreSettingsHTMLElement extends QuotquotElement {
    constructor() {
        super({
            template,
            state: {
                styles: blaze.urlsFromManifest(index),
                events: {
                }
            },
            Renderers: [EventsRenderer, FluentRenderer, ClassRenderer],
            Plugins: [StylePlugin]
        });
    }
}

window.customElements.define(index.tagName, StoreSettingsHTMLElement);
