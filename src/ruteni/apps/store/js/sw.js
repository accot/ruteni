import { precacheAndRoute } from "workbox-precaching";
// import { registerRoute } from "workbox-routing";
// import { CacheFirst } from "workbox-strategies";

// https://developers.google.com/web/tools/workbox/modules/workbox-strategies

precacheAndRoute(self.__WB_MANIFEST);

// function matchFunction({ url }) {
//     return url.pathname.indexOf("/locales/") !== -1 ||
//         url.pathname.startsWith("/app/store/") ||
//         ["/favicon.ico"].includes(url.pathname);
// }

// registerRoute(matchFunction, new CacheFirst());
