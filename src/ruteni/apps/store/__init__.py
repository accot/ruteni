import logging

from asgiref.typing import HTTPScope
from pkg_resources import resource_filename

from ruteni.apis import APINode
from ruteni.core.types import HTTPApp
from ruteni.endpoints import GET
from ruteni.responses.json import JSONResponse
from ruteni.routing import current_path_is
from ruteni.routing.nodes.apps import ProgressiveWebAppNode
from ruteni.routing.nodes.http import HTTPAppMapNode

logger = logging.getLogger(__name__)

app_node = ProgressiveWebAppNode(
    "store", 1, resource_filename(__name__, "resources"), {"fr-FR", "en-US"}
)


async def list_apps(scope: HTTPScope) -> HTTPApp:
    result: list = []
    """
    for app in AppNode.all_apps:
        # ignore the app store itself
        if app is store:
            continue

        # if the app requires special access rights, check that the user statifies them
        if isinstance(app, UserAccessMixin) and not (
            request.user.is_authenticated and app.accessible_to(request.user.id)
        ):
            continue

        if isinstance(app, PWANode):
            locale = get_locale_from_request(request, app.available_locales)
            app_info = app.get_manifest(locale)
        else:
            app_info = dict(name=app.name)

        result.append(app_info)
    """
    return JSONResponse(result)


api_node = APINode(
    "store", 1, [HTTPAppMapNode(current_path_is("/list"), GET(list_apps))]
)
