import logging

from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaPlayer, MediaRelay
from pkg_resources import resource_filename

from ruteni.apis import APINode
from ruteni.config import config
from ruteni.core.types import HTTPApp, HTTPReceive, HTTPScope
from ruteni.endpoints import POST
from ruteni.exceptions import HTTPException
from ruteni.responses.json import JSONResponse
from ruteni.routing import current_path_in, current_path_is
from ruteni.routing.nodes.apps import LocalizedAppNode
from ruteni.routing.nodes.http import HTTPAppMapNode
from ruteni.routing.nodes.http.file import FileNode
from ruteni.utils.form import get_json_body

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

DEVICE: str = config.get("RUTENI_DEVICE", default="/dev/video0")
FRAMERATE: str = config.get("RUTENI_FRAMERATE", cast=int, default=30)
VIDEO_SIZE: str = config.get("RUTENI_VIDEO_SIZE", default="640x480")

options = {"framerate": str(FRAMERATE), "video_size": VIDEO_SIZE}
webcam = MediaPlayer(DEVICE, format="v4l2", options=options)

relay = MediaRelay()


def configure(peer_connection: RTCPeerConnection) -> None:
    peer_connection.addTrack(relay.subscribe(webcam.video))


nodes = (
    FileNode(
        current_path_in(("", "/")),
        resource_filename(__name__, "resources/index.html"),
        content_type=b"text/html",
    ),
)
app_node = LocalizedAppNode("webcam", 1, {"fr-FR", "en-US"}, nodes)


async def webcam_endpoint(scope: HTTPScope, receive: HTTPReceive) -> HTTPApp:
    try:
        form = await get_json_body(scope, receive)
    except HTTPException as exc:
        return exc.response

    offer = RTCSessionDescription(sdp=form["sdp"], type=form["type"])

    peer_connection = RTCPeerConnection()
    peer_connection.addTrack(relay.subscribe(webcam.video))

    await peer_connection.setRemoteDescription(offer)

    answer = await peer_connection.createAnswer()
    await peer_connection.setLocalDescription(answer)

    return JSONResponse(
        {
            "sdp": peer_connection.localDescription.sdp,
            "type": peer_connection.localDescription.type,
        }
    )


# api_node = APINode("webcam", 1, [WebRTCNode(current_path_is("/offer"), configure)])

api_node = APINode(
    "webcam",
    1,
    [
        HTTPAppMapNode(current_path_is("/offer"), POST(webcam_endpoint)),
    ],
)
