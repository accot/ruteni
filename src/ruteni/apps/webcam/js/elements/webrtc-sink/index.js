import ClassRenderer from "external:@totates/quotquot/v1:plugin.class";
import EventsRenderer from "external:@totates/quotquot/v1:plugin.events";
import FluentRenderer from "external:@totates/quotquot/v1:plugin.fluent";
import { QuotquotElement } from "external:@totates/quotquot/v1:element";
import StylePlugin from "external:@totates/quotquot/v1:plugin.style";
import blaze from "external:@totates/blaze-css/v10";
import logging from "external:@totates/logging/v1";
import policy from "external:@totates/trusted-types/v1";
import { registerElementBundleResolver } from "external:@totates/translation/v1";

import html from "./index.html";
import index from "./index.json";

const logger = logging.getLogger("ruteni.plugin.webrtc.element.sink");
// logger.setLevel("DEBUG");

registerElementBundleResolver(index, import.meta.url);

const template = document.createElement("template");
template.innerHTML = policy.createHTML(html);

class WebrtcSinkHTMLElement extends QuotquotElement {
    constructor() {
        const state = {
            styles: blaze.urlsFromManifest(index),
            events: {}
        };
        super({
            template,
            state,
            Renderers: [ClassRenderer, EventsRenderer, FluentRenderer],
            Plugins: [StylePlugin]
        });
    }
    connectedCallback() {
        super.connectedCallback();
        logger.debug(`added ${index.tagName}`);
        this.start().catch(logger.error);
    }

    disconnectedCallback() {
        super.connectedCallback();
        logger.debug(`removed ${index.tagName}`);
    }

    async start() {
        this.peerConnection = new RTCPeerConnection({
            sdpSemantics: "unified-plan"
            // iceServers = [{ urls: ["stun:stun.l.google.com:19302"] }]
        });
        this.peerConnection.addTransceiver("video", { direction: "recvonly" });
        this.peerConnection.addTransceiver("audio", { direction: "recvonly" });

        // register some listeners to help debugging
        this.peerConnection.addEventListener(
            "icegatheringstatechange",
            () => logger.info(this.peerConnection.iceGatheringState),
            false
        );
        logger.info(this.peerConnection.iceGatheringState);

        this.peerConnection.addEventListener(
            "iceconnectionstatechange",
            () => logger.info(this.peerConnection.iceConnectionState),
            false
        );
        logger.info(this.peerConnection.iceConnectionState);

        this.peerConnection.addEventListener(
            "signalingstatechange",
            () => logger.info(this.peerConnection.signalingState),
            false
        );
        logger.info(this.peerConnection.signalingState);

        this.peerConnection.addEventListener("track", e => {
            this.$(e.track.kind).srcObject = e.streams[0];
            logger.info("added", e.track.kind, "track");
        });

        this.peerConnection.addEventListener(
            "connectionstatechange",
            e => logger.info("state:", e.target.signalingState)
        );

        const offer = await this.peerConnection.createOffer();
        this.peerConnection.setLocalDescription(offer);

        if (this.peerConnection.iceGatheringState !== "complete") {
            const event = "icegatheringstatechange";
            await new Promise(resolve => {
                const checkState = () => {
                    if (this.peerConnection.iceGatheringState === "complete") {
                        this.peerConnection.removeEventListener(event, checkState);
                        resolve();
                    }
                };
                this.peerConnection.addEventListener(event, checkState);
            });
        }

        logger.info(offer.sdp);

        const response = await fetch("/api/webcam/v1/offer", {
            body: JSON.stringify({ sdp: offer.sdp, type: offer.type }),
            headers: { "Content-Type": "application/json" },
            method: "POST"
        });

        const answer = await response.json();
        this.peerConnection.setRemoteDescription(answer);
        logger.info(answer.sdp);
    }

    stop() {
        if (this.peerConnection.getTransceivers) {
            for (const transceiver of this.peerConnection.getTransceivers()) {
                if (transceiver.stop)
                    transceiver.stop();
            }
        }

        for (const sender of this.peerConnection.getSenders())
            sender.track.stop();

        setTimeout(() => this.peerConnection.close(), 500);
    }
}

window.customElements.define(index.tagName, WebrtcSinkHTMLElement);
