already-have-an-account = Vous avez déjà un compte ?

at-least-eight-characters =
    .placeholder = Au moins 8 caractères

back-home = Retour à la page d'enregistrement

check-your-mailbox = Nous avons envoyé un message à votre adresse électronique avec un code. Pour vérifier votre adresse, copiez ci-dessous le code qui vous a été envoyé dans les { $minutes } minutes.

code-expired = Le code de vérification a expiré. Veuillez réessayer de vous enregistrer.

create-account = Créer un compte

create-your-account =
    .value = Créez votre compte

delivered = Un message vous a été envoyé

delivery-failed = Nous n'avons pas réussi à envoyer de message à l'adresse que vous avez donnée.

email = Courriel

email-confirmed = Vous avez bien confirmé votre adresse. Votre compte est maintenant actif.

empty-display-name = Le nom ne peut pas être vide

empty-email = L'adresse électronique ne peut pas être vide

empty-locale = La langue ne peut pas être vide

empty-password = Le mot de passe ne peut pas être vide

enter-code = Entrez le code de vérification qui vous a été envoyé par courriel

enter-valid-email = Entrez une adresse valide

enter-your-name = Entrez votre nom

how-to-enable-cookies = Comment activer les cookies

incomplete-email = L'adresse électronique est incomplète

misconfigured-domain-email = Le domaine de l'adresse est mal configuré

overflow-display-name = Le nom est trop long

overflow-email = L'adresse électronique est trop longue

overflow-password = Le mot de passe est trop long

parse-error-email = Le sinkat de l'adresse électronique est incorrect

password = Mot de passe

password-eight-characters = Les mots de passe doivent avoir au moins 8 caractères

password-mismatch = Les mots de passe ne correspondent pas

password-must-match = Les mots de passe doivent être identiques

please-enable-cookies = Veuillez activer les cookies

reenter-password = Ressaisir le mot de passe

server-timeout = Le serveur n'a pas répondu. Veuillez réessayer.

signin = Se connecter

too-many-attempts = Vous avez fait trop d'erreurs. Veuillez réessayer de vous enregistrer.

unknown-domain-email = Le domaine de l'adresse électronique est inconnu

unknown-locale = La langue est inconnue

verify-email =
    .value = Vérifier

weak-password = Le mot de passe est trop faible

wrong-code-length = Mauvaise longueur du code

wrong-code-value = Le code est incorrect. Il vous reste { $remaining } essais.

your-name = Votre nom
