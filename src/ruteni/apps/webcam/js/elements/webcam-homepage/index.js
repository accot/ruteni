import policy from "external:@totates/trusted-types/v1";
import { QuotquotElement } from "external:@totates/quotquot/v1:element";
import EventsRenderer from "external:@totates/quotquot/v1:plugin.events";
import StylePlugin from "external:@totates/quotquot/v1:plugin.style";
import ClassRenderer from "external:@totates/quotquot/v1:plugin.class";

import html from "./index.html";
import index from "./index.json";
import resources from "./resources.js";

const template = document.createElement("template");
template.innerHTML = policy.createHTML(html);

const styles = [
    resources["@ruteni/webcam/v1"]["element.webcam-homepage.style"]
];

class WebcamHomepageHTMLElement extends QuotquotElement {
    constructor() {
        const state = {
            issues: {},
            state: "editing",
            styles,
            events: {
            }
        };
        super({
            template,
            state,
            Renderers: [EventsRenderer, ClassRenderer],
            Plugins: [StylePlugin]
        });
    }
}

window.customElements.define(index.tagName, WebcamHomepageHTMLElement);
