module.exports = {
    input: [
        "components/homepage/index.html"
    ],
    output: ".",
    options:
    {
        debug: false,
        removeUnusedKeys: true,
        sort: true,
        attr: {
            list: ["data-i18n"],
            extensions: [".html"]
        },
        lngs: ["en", "fr"],
        ns: ["webcam", "translation"],
        defaultLng: "en",
        defaultNs: "webcam",
        defaultValue: "",
        resource: {
            loadPath: "locales/{{ns}}/{{lng}}.json",
            savePath: "locales/{{ns}}/{{lng}}.json",
            jsonIndent: 4
        },
        nsSeparator: ":",
        keySeparator: ".",
        pluralSeparator: "_",
        contextSeparator: "_",
        contextDefaultValues: [],
        interpolation: {
            prefix: "{{",
            suffix: "}}"
        }
    }
};
