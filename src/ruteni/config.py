import logging
import os
from typing import Any

from starlette.config import Config as StarletteConfig

logger = logging.getLogger(__name__)

environ = {}


class Config(StarletteConfig):
    def set(self, key: str, value: Any) -> None:
        environ[key] = value  # starlette's environment cannot be changed

    @property
    def env(self) -> str:
        return self.get("RUTENI_ENV", default="production")

    @property
    def is_devel(self) -> bool:
        return self.env == "development"

    @property
    def is_debug(self) -> bool:
        return self.get("RUTENI_DEBUG", cast=bool, default=False)


config = Config(env_file=os.environ.get("RUTENI_CONFIG", ".env"), environ=environ)
