from collections.abc import Collection, Generator, Iterable
from typing import Optional

from asgiref.typing import HTTPScope
from ruteni.routing.types import URLPath
from werkzeug.datastructures import LanguageAccept
from werkzeug.http import parse_accept_header

from ruteni.routing.types import AcceptRoute, FileMatch, PathLike, Route, RouteParam


def current_path(route: Route) -> URLPath:
    return route[-1][0]


def current_path_is(url_path: URLPath) -> AcceptRoute:
    return lambda route: current_path(route) == url_path


def current_path_in(url_paths: Collection[URLPath]) -> AcceptRoute:
    return lambda route: current_path(route) in url_paths


def language_match(
    url_path: URLPath, languages: Iterable[str], file_path_pattern: str
) -> FileMatch:
    url_match = current_path_is(url_path)

    def match(scope: HTTPScope, route: Route) -> Optional[PathLike]:
        if not url_match(route):
            return None
        for key, val in scope["headers"]:
            if key == b"accept-language":
                language_accept = parse_accept_header(val.decode(), LanguageAccept)
                language = language_accept.best_match(languages)
                break
        else:
            language = None
        return file_path_pattern.format(language or "en-US")

    return match


def get_route_params(
    route: Route, count: Optional[int] = None
) -> Generator[Optional[RouteParam], None, None]:
    for index, route_elem in enumerate(reversed(route)):
        if index == count:
            return
        yield route_elem[1]
