import os
from collections.abc import Awaitable, Callable, MutableSequence
from typing import Literal, Optional, Protocol, Tuple, TypeVar, Union
from uuid import UUID

from asgiref.typing import HTTPScope, WebSocketScope
from starlette.datastructures import URLPath

from ruteni.core.types import App, HTTPApp, WebSocketApp

PathLike = Union[str, os.PathLike[str]]

Header = Tuple[bytes, bytes]
HeadersProvider = Callable[[HTTPScope], Awaitable[Tuple[Header, ...]]]

Content = Tuple[bytes, bytes]
ContentProvider = Callable[[HTTPScope], Awaitable[Content]]

AtomTypeName = Literal["str", "path", "int", "float", "uuid"]
Atom = Union[str, URLPath, int, float, UUID]
RouteParam = Union[Atom, Tuple[Atom, ...]]
RouteElem = tuple[URLPath, Optional[RouteParam]]
Route = MutableSequence[RouteElem]
Extractor = Callable[[URLPath], Optional[RouteElem]]

HTTPNode = Callable[[HTTPScope, Route], Awaitable[Optional[HTTPApp]]]
HTTPRouter = Callable[[HTTPScope], HTTPApp]

WebSocketNode = Callable[[WebSocketScope, Route], Awaitable[Optional[WebSocketApp]]]
WebSocketRouter = Callable[[WebSocketScope], WebSocketApp]

TScope = TypeVar("TScope", HTTPScope, WebSocketScope)

Node = Callable[[TScope, Route], Awaitable[Optional[App]]]
Router = Callable[[TScope], App]

# from starlette.convertors import CONVERTOR_TYPES
# CONVERTOR_TYPES keys: ["str", "path", "int", "float", "uuid"]
# from werkzeug.routing import DEFAULT_CONVERTERS
# DEFAULT_CONVERTERS keys: ["default", "string", "any", "path", "int", "float", "uuid"]
AcceptRoute = Callable[[Route], bool]
FileMatch = Callable[[HTTPScope, Route], Optional[PathLike]]


class ParamReadEndpoint(Protocol):
    async def __call__(self, scope: TScope, *args: Atom) -> App: ...
