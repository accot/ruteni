import re
from typing import Optional, cast
from uuid import UUID

from ruteni.routing.types import URLPath

from ruteni.routing.types import RouteElem


def path_extractor(url_path: URLPath) -> Optional[RouteElem]:
    return (cast(URLPath, ""), url_path[1:]) if url_path.startswith("/") else None


def uuid_extractor(url_path: URLPath) -> Optional[RouteElem]:
    # 37 is 1 ("/") + 36 ("74d08497-d15a-486d-aef2-588d12bebdbb")
    if (
        len(url_path) >= 37
        and url_path[0] == "/"
        and (len(url_path) == 37 or url_path[37] == "/")
    ):
        try:
            uuid = UUID(url_path[1:37])
        except ValueError:
            pass
        else:
            return (cast(URLPath, url_path[37:]), uuid)
    return None


def int_extractor(url_path: URLPath) -> Optional[RouteElem]:
    parts = url_path.split("/", 2)
    if len(parts) >= 2 and parts[0] == "":  # i.e. url_path[0] == "/"
        try:
            value = int(parts[1])
        except ValueError:
            pass
        else:
            return (cast(URLPath, "/" + parts[2] if len(parts) > 2 else ""), value)
    return None


def float_extractor(url_path: URLPath) -> Optional[RouteElem]:
    parts = url_path.split("/", 2)
    if len(parts) >= 2 and parts[0] == "":  # i.e. url_path[0] == "/"
        try:
            value = float(parts[1])
        except ValueError:
            pass
        else:
            return (cast(URLPath, "/" + parts[2] if len(parts) > 2 else ""), value)
    return None


def str_extractor(url_path: URLPath) -> Optional[RouteElem]:
    parts = url_path.split("/", 2)
    return (
        (cast(URLPath, "/" + parts[2] if len(parts) > 2 else ""), parts[1])
        if len(parts) >= 2 and parts[0] == "" and len(parts[1])
        else None
    )


type_extractors = {
    "str": str_extractor,
    "path": path_extractor,
    "int": int_extractor,
    "float": float_extractor,
    "uuid": uuid_extractor,
}


class PrefixExtractor:
    def __init__(self, prefix: URLPath) -> None:
        self.prefix = prefix

    def __repr__(self) -> str:
        return "%s(prefix=%r)" % (self.__class__.__name__, self.prefix)

    def __call__(self, url_path: URLPath) -> Optional[RouteElem]:
        return (
            (cast(URLPath, url_path.removeprefix(self.prefix)), self.prefix)
            if url_path == self.prefix or url_path.startswith(self.prefix + "/")
            else None
        )


class RegexExtractor:
    def __init__(self, pattern: URLPath) -> None:
        self.pattern = pattern + "(/.*)"
        self.prog = re.compile(pattern)

    def __repr__(self) -> str:
        return "%s(pattern=%r)" % (self.__class__.__name__, self.pattern)

    def __call__(self, url_path: URLPath) -> Optional[RouteElem]:
        result = self.prog.fullmatch(url_path)
        if result is None:
            return None
        groups = result.groups()
        return (cast(URLPath, groups[-1]), groups[:-1])
