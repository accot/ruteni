from __future__ import annotations

import logging
import re
from collections.abc import Iterable, Mapping
from typing import Generic, Optional, TypeVar

from asgiref.typing import HTTPScope, WebSocketScope
from ruteni.routing.types import URLPath
from ruteni.responses.redirect import RedirectResponse

from ruteni.core.types import App
from ruteni.responses.errors import (
    HTTP_404_NOT_FOUND_RESPONSE,
    HTTP_405_METHOD_NOT_ALLOWED_RESPONSE,
)
from ruteni.routing import current_path, get_route_params
from ruteni.routing.extractors import PrefixExtractor, type_extractors
from ruteni.routing.types import AcceptRoute, Extractor, Node, ParamReadEndpoint, Route

logger = logging.getLogger(__name__)


TScope = TypeVar("TScope", HTTPScope, WebSocketScope)


class IterableNode(Generic[TScope]):
    def __init__(self, nodes: Iterable[Node]) -> None:
        # TODO: assert nodes can be iterated multiple times (e.g. not a generator)
        self.nodes = nodes

    def __repr__(self) -> str:
        return "%s()" % self.__class__.__name__

    async def __call__(self, scope: TScope, route: Route) -> Optional[App]:
        for node in self.nodes:
            response = await node(scope, route)
            if response is not None:
                return response
        return None


class MappingNode(Generic[TScope]):
    def __init__(self, node_map: Mapping[URLPath, Node]) -> None:
        self.node_map = node_map

    def __repr__(self) -> str:
        return "%s()" % self.__class__.__name__

    async def __call__(self, scope: TScope, route: Route) -> Optional[App]:
        url_path = current_path(route)
        for prefix, node in self.node_map.items():
            if url_path == prefix or url_path.startswith(prefix + "/"):
                route_elem = (url_path.removeprefix(prefix), prefix)
                route.append(route_elem)
                response = await node(scope, route)
                if response is None:
                    route.pop()
                return response
        return None


class ExtractorNode(Generic[TScope]):
    def __init__(self, extractor: Extractor, child: Node) -> None:
        self.extractor = extractor
        self.child = child

    def __repr__(self) -> str:
        return super().__repr__()[:-1] + ", extractor=%r)" % self.extractor

    async def __call__(self, scope: TScope, route: Route) -> Optional[App]:
        route_elem = self.extractor(current_path(route))
        if route_elem is None:
            return None
        route.append(route_elem)
        response = await self.child(scope, route)
        if response is None:
            route.pop()
        return response


def path_to_node2(path: str, endpoint: Node, i: int = 0) -> Node:
    if path in ("", "/"):
        return endpoint
    m = re.match(r"((/[^/{}]+)|/\{([^/{}]+)\})(.*)", path)
    assert m, "invalid path"
    prefix = m.group(2)
    rest = m.group(4)
    if prefix:
        assert prefix, f"empty path prefix in {path}"
        return ExtractorNode(PrefixExtractor(prefix), path_to_node2(rest, endpoint, i))
    else:
        param = m.group(3)
        assert param in type_extractors, f"unknown param type '{param}'"
        return ExtractorNode(
            type_extractors[param], path_to_node2(rest, endpoint, i + 1)
        )


def path_to_node(path: str, endpoint: ParamReadEndpoint, i: int = 0) -> Node:
    if path in ("", "/"):

        async def node(scope: HTTPScope, route: Route) -> Optional[App]:
            url_path = current_path(route)

            if url_path != "" and url_path != "/":
                return HTTP_404_NOT_FOUND_RESPONSE

            if scope["method"] != "GET":
                return HTTP_405_METHOD_NOT_ALLOWED_RESPONSE

            return await endpoint(scope, *get_route_params(route, i))

        return node

    m = re.match(r"((/[^/{}]+)|/\{([^/{}]+)\})(.*)", path)
    assert m, "invalid path"
    prefix = m.group(2)
    rest = m.group(4)
    if prefix:
        assert prefix, f"empty path prefix in {path}"
        return ExtractorNode(PrefixExtractor(prefix), path_to_node(rest, endpoint, i))
    else:
        param = m.group(3)
        assert param in type_extractors, f"unknown param type '{param}'"
        return ExtractorNode(
            type_extractors[param], path_to_node(rest, endpoint, i + 1)
        )


class RedirectNode(Generic[TScope]):
    def __init__(self, accept_route: AcceptRoute, url_path: URLPath) -> None:
        self.accept_route = accept_route
        self.url_path = url_path

    def __repr__(self) -> str:
        return "%s(url_path=%r)" % (self.__class__.__name__, self.url_path)

    async def __call__(self, scope: TScope, route: Route) -> Optional[App]:
        return RedirectResponse(self.url_path) if self.accept_route(route) else None


class SlashRedirectNode(Generic[TScope]):
    def __repr__(self) -> str:
        return "%s()" % (self.__class__.__name__)

    async def __call__(self, scope: TScope, route: Route) -> Optional[App]:
        return (
            RedirectResponse(scope["path"] + "/") if current_path(route) == "" else None
        )
