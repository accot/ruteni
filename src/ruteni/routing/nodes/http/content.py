from typing import Optional

from asgiref.typing import HTTPScope

from ruteni.core.types import App
from ruteni.responses import Response
from ruteni.responses.errors import HTTP_405_METHOD_NOT_ALLOWED_RESPONSE
from ruteni.routing.types import AcceptRoute, ContentProvider, Route


class ContentNode:
    def __init__(
        self, accept_route: AcceptRoute, content: bytes, content_type: bytes
    ) -> None:
        self.accept_route = accept_route
        self.response = Response(content, content_type)

    def __repr__(self) -> str:
        return "%s()" % self.__class__.__name__

    async def __call__(self, scope: HTTPScope, route: Route) -> Optional[App]:
        if not self.accept_route(route):
            return None
        return (
            self.response
            if scope["method"] == "GET"
            else HTTP_405_METHOD_NOT_ALLOWED_RESPONSE
        )


class ContentProviderNode:
    def __init__(self, accept_route: AcceptRoute, provider: ContentProvider) -> None:
        self.accept_route = accept_route
        self.provider = provider

    def __repr__(self) -> str:
        return "%s()" % self.__class__.__name__

    async def __call__(self, scope: HTTPScope, route: Route) -> Optional[App]:
        if not self.accept_route(route):
            return None
        if scope["method"] != "GET":
            return HTTP_405_METHOD_NOT_ALLOWED_RESPONSE
        content, content_type = await self.provider(scope)
        return Response(content, content_type)
