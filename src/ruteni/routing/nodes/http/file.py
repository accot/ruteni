import os
import stat
from typing import Optional, Tuple

from anyio import Path
from asgiref.typing import HTTPScope

from ruteni.core.types import App
from ruteni.responses.errors import (
    HTTP_405_METHOD_NOT_ALLOWED_RESPONSE,
    HTTP_500_INTERNAL_SERVER_ERROR_RESPONSE,
)
from ruteni.responses.file import FileResponse
from ruteni.routing.types import (
    AcceptRoute,
    FileMatch,
    Header,
    HeadersProvider,
    PathLike,
    Route,
)


class FrozenFileNode:
    def __init__(
        self,
        accept_route: AcceptRoute,
        path: PathLike,
        content_type: bytes,
        *,
        headers: Tuple[Header, ...] = (),
    ) -> None:
        self.stat_result = os.stat(path)
        assert stat.S_ISREG(self.stat_result.st_mode)
        # TODO: if the file changes, stat_result has a wrong size, the response fails
        # watch changes?
        # from ruteni.services.inotify import inotify_service
        # self.watch = inotify_service.watch(path)

        self.accept_route = accept_route
        self.path = path
        self.content_type = content_type
        self.response = FileResponse(
            self.path,
            stat_result=self.stat_result,
            content_type=self.content_type,
            headers=headers,
        )

    def __repr__(self) -> str:
        return "%s(path=%r, content_type=%r, content_length=%r)" % (
            self.__class__.__name__,
            self.path,
            self.content_type,
            self.stat_result.st_size,
        )

    async def __call__(self, scope: HTTPScope, route: Route) -> Optional[App]:
        if not self.accept_route(route):
            return None
        return (
            self.response
            if scope["method"] == "GET"
            else HTTP_405_METHOD_NOT_ALLOWED_RESPONSE
        )


async def send_file(
    scope: HTTPScope,
    path: PathLike,
    content_type: bytes,
    headers_provider: Optional[HeadersProvider] = None,
) -> App:
    if scope["method"] != "GET":
        return HTTP_405_METHOD_NOT_ALLOWED_RESPONSE

    try:
        stat_result = await Path(path).stat()
    except FileNotFoundError:
        return HTTP_500_INTERNAL_SERVER_ERROR_RESPONSE

    if not stat.S_ISREG(stat_result.st_mode):
        return HTTP_500_INTERNAL_SERVER_ERROR_RESPONSE  # wrong node configuration

    headers = await headers_provider(scope) if headers_provider else ()

    return FileResponse(
        path, stat_result=stat_result, content_type=content_type, headers=headers
    )


class FileNode:
    def __init__(
        self,
        accept_route: AcceptRoute,
        path: PathLike,
        content_type: bytes,
        *,
        headers_provider: Optional[HeadersProvider] = None,
    ) -> None:
        # TODO: remember stat_result? if the file is updated, the response will be wrong
        stat_result = os.stat(path)
        assert stat.S_ISREG(stat_result.st_mode)
        self.accept_route = accept_route
        self.path = path
        self.content_type = content_type
        self.headers_provider = headers_provider

    def __repr__(self) -> str:
        return "%s(path=%r, content_type=%r)" % (
            self.__class__.__name__,
            self.path,
            self.content_type,
        )

    async def __call__(self, scope: HTTPScope, route: Route) -> Optional[App]:
        return (
            await send_file(scope, self.path, self.content_type, self.headers_provider)
            if self.accept_route(route)
            else None
        )


class CustomFileNode:
    def __init__(
        self,
        match: FileMatch,
        content_type: bytes,
        *,
        headers_provider: Optional[HeadersProvider] = None,
    ) -> None:
        self.match = match
        self.content_type = content_type
        self.headers_provider = headers_provider

    def __repr__(self) -> str:
        return "%s(content_type=%r)" % (self.__class__.__name__, self.content_type)

    async def __call__(self, scope: HTTPScope, route: Route) -> Optional[App]:
        # TODO: the matching function probably needs to be able to return an error
        path = self.match(scope, route)
        return (
            await send_file(scope, path, self.content_type, self.headers_provider)
            if path
            else None
        )
