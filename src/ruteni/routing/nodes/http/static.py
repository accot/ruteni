import os
import stat
from collections.abc import Iterable
from mimetypes import guess_type
from typing import Optional

from anyio import Path
from asgiref.typing import HTTPScope

from ruteni.core.types import App
from ruteni.responses.errors import (
    HTTP_400_BAD_REQUEST_RESPONSE,
    HTTP_404_NOT_FOUND_RESPONSE,
    HTTP_405_METHOD_NOT_ALLOWED_RESPONSE,
)
from ruteni.responses.file import FileResponse
from ruteni.routing.nodes import current_path
from ruteni.routing.types import PathLike, Route


class StaticHTTPNode:
    def __init__(self, directories: Iterable[PathLike]) -> None:
        self.directories = [os.path.realpath(directory) for directory in directories]
        for directory in self.directories:
            stat_result = os.stat(directory)
            assert stat.S_ISDIR(stat_result.st_mode)

    def __repr__(self) -> str:
        root_dir = os.path.dirname(os.path.dirname(__file__))  # TODO: fragile
        relpaths = [
            os.path.relpath(directory, root_dir) for directory in self.directories
        ]
        return "%s(directories=%r)" % (self.__class__.__name__, relpaths)

    async def __call__(self, scope: HTTPScope, route: Route) -> Optional[App]:
        url_path = current_path(route)
        if len(url_path) == 0 or url_path[0] != "/":
            return HTTP_400_BAD_REQUEST_RESPONSE

        if scope["method"] != "GET":
            return HTTP_405_METHOD_NOT_ALLOWED_RESPONSE

        for directory in self.directories:
            full_path = os.path.realpath(os.path.join(directory, url_path[1:]))

            # get the stat info for the file, and detect if it does not exist
            try:
                stat_result = await Path(full_path).stat()
            except FileNotFoundError:
                continue  # try other directories

            if not stat.S_ISREG(stat_result.st_mode):
                return HTTP_400_BAD_REQUEST_RESPONSE

            # the path exists; test that the path is indeed a descendant of `directory`
            # to prevent requests using `../..` from going too far up the hierarchy
            if os.path.commonprefix([full_path, directory]) != directory:
                return HTTP_400_BAD_REQUEST_RESPONSE

            mime_type, encoding = guess_type(full_path)
            content_type = mime_type or "text/plain"  # TODO: use encoding

            # return its content if it's a regular file, or error 400 otherwise
            return FileResponse(
                full_path,
                stat_result=stat_result,
                content_type=content_type.encode("latin-1"),
            )

        return HTTP_404_NOT_FOUND_RESPONSE  # found in no static directory
