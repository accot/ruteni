import os
from collections.abc import Iterable
from typing import Optional

from ruteni.apis.security import SecurityHeadersProvider
from ruteni.plugins.sessions import ensure_session_headers_provider
from ruteni.routing import current_path_is, language_match
from ruteni.routing.nodes import IterableNode, SlashRedirectNode
from ruteni.routing.nodes.http.file import CustomFileNode, FileNode, FrozenFileNode
from ruteni.routing.types import Node, PathLike
from ruteni.services import Service, ServiceSet
from ruteni.utils.headers import HeadersGatherer


class AppNode(Service, IterableNode):
    def __init__(
        self,
        name: str,
        version: int,
        nodes: Iterable[Node],
        requires: Optional[ServiceSet] = None,
    ) -> None:
        Service.__init__(self, name, version, requires)
        IterableNode.__init__(self, nodes)


class LocalizedAppNode(AppNode):
    def __init__(
        self,
        name: str,
        version: int,
        languages: set[str],
        nodes: Iterable[Node],
        requires: Optional[ServiceSet] = None,
    ) -> None:
        super().__init__(name, version, nodes, requires)
        self.languages = languages


# https://medium.com/@applification/progressive-web-app-splash-screens-80340b45d210


class ProgressiveWebAppNode(LocalizedAppNode):
    def __init__(
        self,
        name: str,
        version: int,
        resources_dir: PathLike,
        languages: set[str],
        *,
        connect: bool = True,  # TODO: find good default values
        img: bool = True,
        manifest: bool = True,
        script: bool = True,
        style: bool = True,
        worker: bool = True,
    ) -> None:
        # TODO: check that all resources exist
        super().__init__(
            name,
            version,
            languages,
            (
                FileNode(
                    current_path_is("/"),
                    os.path.join(resources_dir, "index.html"),
                    headers_provider=HeadersGatherer(
                        ensure_session_headers_provider,
                        SecurityHeadersProvider(
                            connect=connect,
                            img=img,
                            manifest=manifest,
                            script=script,
                            style=style,
                            worker=worker,
                        ),
                    ),
                    content_type=b"text/html",
                ),
                CustomFileNode(
                    language_match(
                        "/sw.js", languages, os.path.join(resources_dir, "{}/sw.js")
                    ),
                    content_type=b"application/javascript",
                ),
                CustomFileNode(
                    language_match(
                        "/manifest.json",
                        languages,
                        os.path.join(resources_dir, "{}/manifest.json"),
                    ),
                    content_type=b"application/manifest+json",
                ),
                CustomFileNode(
                    language_match(
                        "/resources.json",
                        languages,
                        os.path.join(resources_dir, "{}/resources.json"),
                    ),
                    content_type=b"application/json",
                ),
                FrozenFileNode(
                    current_path_is("/favicon.ico"),
                    os.path.join(resources_dir, "favicon.ico"),
                    content_type=b"image/x-icon",
                ),
                SlashRedirectNode(),
            ),
        )
