import logging
from collections.abc import Callable

from asgiref.typing import HTTPScope

from ruteni.apis import APINode
from ruteni.core.types import HTTPApp
from ruteni.endpoints import GET
from ruteni.plugins.identity import _dbr_get_user_from_headers
from ruteni.responses.errors import HTTP_403_FORBIDDEN_RESPONSE
from ruteni.responses.json import JSONResponse
from ruteni.routing import current_path_is
from ruteni.routing.nodes.http import HTTPAppMapNode
from ruteni.services.database import database
from ruteni.services.locales import locales_service

logger = logging.getLogger(__name__)


UserTest = Callable[[int], bool]


class UserAccessMixin:
    def __init__(self, accessible_to: UserTest) -> None:
        self.accessible_to = accessible_to


async def user_info(scope: HTTPScope) -> HTTPApp:
    async with database.connect() as conn:
        user = await _dbr_get_user_from_headers(conn, scope["headers"])
    return JSONResponse(user.to_dict()) if user else HTTP_403_FORBIDDEN_RESPONSE


# TODO: @requires("authenticated")

api_node = APINode(
    "user",
    1,
    [HTTPAppMapNode(current_path_is("/info"), GET(user_info))],
    {database, locales_service},
)
