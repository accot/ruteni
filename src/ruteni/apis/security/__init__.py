import logging
from collections.abc import Iterable
from typing import Optional

from asgiref.typing import HTTPScope, Tuple

from ruteni.apis import APINode
from ruteni.config import config
from ruteni.core.types import HTTPApp, HTTPReceive
from ruteni.endpoints import POST
from ruteni.exceptions import HTTPException
from ruteni.models.security_reports import ReportType, security_reports
from ruteni.responses.json import JSONResponse
from ruteni.routing import current_path_is
from ruteni.routing.nodes.http import HTTPAppMapNode
from ruteni.routing.types import Header
from ruteni.services.database import database
from ruteni.utils.form import get_json_body

logger = logging.getLogger(__name__)


# TODO: add middleware that checks that csp header is set on html?
# https://web.dev/csp-xss/
# https://flask.palletsprojects.com/en/2.0.x/security/
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security
# https://github.com/shieldfy/APINode-Security-Checklist/blob/master/README.md
# https://mechlab-engineering.de/2019/01/security-testing-and-deployment-of-an-api-release-your-flask-app-to-the-internet/
# https://pypi.org/project/asgi-sage/
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security
# https://hstspreload.org/

HSTS_MAX_AGE: int = config.get(
    "RUTENI_SECURITY_HSTS_MAX_AGE", cast=int, default=63072000  # 2 years
)

CSP_REPORT_CONTENT_TYPE = b"application/csp-report"
CSP_REPORT_KEY = "csp-report"


async def csp_report(scope: HTTPScope, receive: HTTPReceive) -> HTTPApp:
    """
    Example of CSP report:
    {
        "csp-report": {
            "document-uri": "http://localhost:8000/app/store/",
            "referrer": "",
            "violated-directive": "script-src-elem",
            "effective-directive": "script-src-elem",
            "original-policy": "default-src 'self'; worker-src 'self'; frame-src 'none'; child-src 'none'; object-src 'none'; require-trusted-types-for 'script'; report-uri /api/security/v1/csp-report/;",
            "disposition": "enforce",
            "blocked-uri": "inline",
            "line-number": 16,
            "source-file": "http://localhost:8000/app/store/",
            "status-code": 200,
            "script-sample": "",
        }
    }
    """
    try:
        pkt = await get_json_body(scope, receive, CSP_REPORT_CONTENT_TYPE)
    except HTTPException as exc:
        return exc.response
    report = pkt[CSP_REPORT_KEY]
    # user_id = request.user.id if request.user.is_authenticated else None
    async with database.begin() as conn:
        await conn.execute(
            security_reports.insert().values(type=ReportType.CSP, report=report)
        )
    return JSONResponse(True)


api_node = APINode(
    "security",
    1,
    [
        HTTPAppMapNode(current_path_is("/csp-report"), POST(csp_report)),
    ],
    {database},
)

# TODO: generate CSP_REPORT_URI from api_node
CSP_REPORT_URI = b"/api/security/v1/csp-report"  # csp_report_endpoint.url_path

STRICT_TRANSPORT_SECURITY = (
    b"Strict-Transport-Security",
    b"max-age=" + str(HSTS_MAX_AGE).encode() + b"; includeSubDomains; preload",
)
X_CONTENT_TYPE_OPTIONS = (b"X-Content-Type-Options", b"nosniff")
X_FRAME_OPTIONS = (b"X-Frame-Options", b"deny")
X_XSS_PROTECTION = (b"X-XSS-Protection", b"1; mode=block")
ACCESS_CONTROL_ALLOW_METHODS = (b"Access-Control-Allow-Methods", b"GET")


def get_security_headers(
    connect: bool = False,
    img: bool = False,
    manifest: bool = False,
    script: bool = False,
    style: bool = False,
    worker: bool = False,
    csp_report_only: bool = False,
    trusted_types: Optional[Iterable[bytes]] = (b"default", b"dompurify", b"totates"),
) -> Tuple[Header, ...]:
    # https://csp.withgoogle.com/docs/strict-csp.html
    csp = b"report-uri " + CSP_REPORT_URI + b"; base-uri 'none'; default-src 'none'"
    if connect:
        csp += b"; connect-src 'self'"
    if img:
        csp += b"; img-src 'self'"
    if manifest:
        csp += b"; manifest-src 'self'"
    if script:
        csp += b"; script-src 'self'"
    if style:
        csp += b"; style-src 'self'"
    if worker:
        csp += b"; worker-src 'self' blob:"  # TODO: blob: is for maplibre; make generic
    if trusted_types:
        csp += b"; require-trusted-types-for 'script'"
        csp += b"; trusted-types " + b" ".join(trusted_types)

    return (
        (
            (
                b"Content-Security-Policy-Report-Only"
                if csp_report_only
                else b"Content-Security-Policy"
            ),
            csp,
        ),
        STRICT_TRANSPORT_SECURITY,
        X_CONTENT_TYPE_OPTIONS,
        X_FRAME_OPTIONS,
        X_XSS_PROTECTION,
        ACCESS_CONTROL_ALLOW_METHODS,
    )


class SecurityHeadersProvider:
    def __init__(
        self,
        connect: bool = False,
        img: bool = False,
        manifest: bool = False,
        script: bool = False,
        style: bool = False,
        worker: bool = False,
    ) -> None:
        self.headers = get_security_headers(
            connect=connect,
            img=img,
            manifest=manifest,
            script=script,
            style=style,
            worker=worker,
        )

    async def __call__(self, scope: HTTPScope) -> Tuple[Header, ...]:
        return self.headers
