import logging from "external:@totates/logging/v1";
import setupAnalytics from "external:@totates/analytics/v1";

import component from "./component.json";// assert { type: "json" };
const logger = logging.getLogger(component.name);
logger.setLevel("DEBUG");

// TODO: parameter for /analytics url
const URL = "/api/analytics/v1/submit";

async function sendMetric(metric) {
    const body = JSON.stringify(metric);
    logger.debug(`sending metric: ${body}`);
    if (navigator.sendBeacon)
        await navigator.sendBeacon(URL, body);
    else
        await fetch(URL, { body, method: "POST", keepalive: true });
}

setupAnalytics(metric => sendMetric(metric).catch(logger.error));
