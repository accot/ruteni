import json
import logging

from asgiref.typing import HTTPScope
from ruteni.routing.types import URLPath

from ruteni.apis import APINode
from ruteni.core.types import HTTPApp, HTTPReceive
from ruteni.endpoints import POST
from ruteni.exceptions import HTTPException
from ruteni.models.analytics import analytics
from ruteni.plugins.sessions import Session
from ruteni.responses import Response
from ruteni.routing import current_path_is
from ruteni.routing.nodes.http import HTTPAppMapNode
from ruteni.services.database import database
from ruteni.utils.form import get_json_body

logger = logging.getLogger(__name__)


async def analytics_app(scope: HTTPScope, receive: HTTPReceive) -> HTTPApp:
    try:
        metric = await get_json_body(scope, receive)
    except HTTPException as exc:
        return exc.response

    # metric example:
    # {
    #     "name": "LCP",
    #     "value": 386.8000000002794,
    #     "rating": "good",
    #     "delta": 386.8000000002794,
    #     "entries": [
    #         {
    #             "name": "",
    #             "entryType": "largest-contentful-paint",
    #             "startTime": 386.8000000002794,
    #             "duration": 0,
    #             "size": 4104,
    #             "renderTime": 386.8000000002794,
    #             "loadTime": 0,
    #             "firstAnimatedFrameTime": 0,
    #             "id": "",
    #             "url": "",
    #         }
    #     ],
    #     "id": "v3-1700156227207-7688676035435",
    #     "navigationType": "navigate",
    # }
    # {
    #     "name": "FID",
    #     "value": 2,
    #     "rating": "good",
    #     "delta": 2,
    #     "entries": [
    #         {
    #             "name": "keydown",
    #             "entryType": "first-input",
    #             "startTime": 3685.2000000001863,
    #             "duration": 8,
    #             "processingStart": 3687.2000000001863,
    #             "processingEnd": 3687.600000000093,
    #             "cancelable": true,
    #         }
    #     ],
    #     "id": "v3-1700156227207-2639087155546",
    #     "navigationType": "navigate",
    # }

    # TODO: session_id should be not none
    session = Session.from_headers(scope["headers"])

    if session:
        async with database.begin() as conn:
            await conn.execute(
                analytics.insert().values(session_id=session["id"], metric=metric)
            )
    else:
        logger.warning("analytics without session")

    return Response(b"", b"text/plain")


api_node = APINode(
    "analytics",
    1,
    [HTTPAppMapNode(current_path_is(URLPath("/submit")), POST(analytics_app))],
)
