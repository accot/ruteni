import logging from "external:@totates/logging/v1";

logging.on("*", (level, logger, args) => {
    console.log("LOGGING", level, args);
});

/*
const defaults = {
    url: "/api/logging/v1/log",
    method: "POST",
    headers: {},
    token: "",
    onUnauthorized: failedToken => {},
    timeout: 0,
    interval: 1000,
    level: "warn",
    backoff: {
        multiplier: 2,
        jitter: 0.1,
        limit: 30000
    },
    capacity: 500,
    stacktrace: {
        levels: ["trace", "warn", "error"],
        depth: 3,
        excess: 0
    },
    timestamp: () => new Date().toISOString(),
    format: remote.json
};
*/

export default logging;
