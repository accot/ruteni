import logging

from asgiref.typing import HTTPScope

from ruteni.apis import APINode
from ruteni.core.types import HTTPApp
from ruteni.endpoints import GET
from ruteni.plugins.sessions import SESSION_ID_NAME, Session
from ruteni.responses import Response
from ruteni.routing import current_path_is
from ruteni.routing.nodes.http import HTTPAppMapNode
from ruteni.services.database import database
from ruteni.services.server import server

logger = logging.getLogger(__name__)


async def register(scope: HTTPScope) -> HTTPApp:
    session = Session.from_headers(scope["headers"])
    if not session:
        session = await Session.create(scope["client"][0] if scope["client"] else None)

    return Response(
        content=str(session[SESSION_ID_NAME]).encode(),
        content_type=b"application/json",
        headers=(session.get_header(),),
    )


api_node = APINode(
    "sessions",
    1,
    [HTTPAppMapNode(current_path_is("/register"), GET(register))],
    requires={database, server},
)
