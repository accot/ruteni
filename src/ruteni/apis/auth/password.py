import logging
from datetime import datetime

from asgiref.typing import HTTPScope
from marshmallow import Schema, validate
from marshmallow.fields import Email, String
from sqlalchemy import and_

from ruteni.apis import APINode
from ruteni.core.types import HTTPApp, HTTPReceive
from ruteni.endpoints import GET, POST
from ruteni.exceptions import HTTPException
from ruteni.models.identities import AuthType, identities
from ruteni.plugins.identity import IDENTITY_SESSION_NAME
from ruteni.plugins.passwords import PASSWORD_MAX_LENGTH, _dbr_check_password
from ruteni.plugins.sessions import Session
from ruteni.plugins.users import User
from ruteni.responses.errors import (
    HTTP_400_BAD_REQUEST_RESPONSE,
    HTTP_401_UNAUTHORIZED_RESPONSE,
)
from ruteni.responses.json import JSONResponse
from ruteni.responses.redirect import RedirectResponse
from ruteni.routing import current_path_is
from ruteni.routing.nodes.http import HTTPAppMapNode
from ruteni.services.database import database
from ruteni.services.server import server
from ruteni.utils.form import get_form
from ruteni.utils.headers import get_content_type_and_auth_headers


logger = logging.getLogger(__name__)


class SignInSchema(Schema):
    email = Email(required=True)
    password = String(required=True, validate=validate.Length(max=PASSWORD_MAX_LENGTH))


async def signin(scope: HTTPScope, receive: HTTPReceive) -> HTTPApp:
    content_type, cookie_value = get_content_type_and_auth_headers(scope)
    if not cookie_value:
        return HTTP_400_BAD_REQUEST_RESPONSE  # TODO: which status code?

    session = Session.from_cookie(cookie_value.decode("latin-1"))
    if not session:
        return HTTP_400_BAD_REQUEST_RESPONSE  # TODO: which status code?

    if "user" in session:
        # already logged in
        # TODO: http://stackoverflow.com/questions/18263796
        return HTTP_400_BAD_REQUEST_RESPONSE  # TODO: which status code?

    try:
        form = await get_form(content_type, receive, SignInSchema)
    except HTTPException as exc:
        return exc.response

    async with database.begin() as conn:
        user_id = await _dbr_check_password(conn, form["email"], form["password"])

        if user_id is None:  # TODO: not clear what status code should be used
            # TODO: log failure
            return HTTP_401_UNAUTHORIZED_RESPONSE

        user = await User._dbr_get_by_id(conn, user_id)
        assert user
        # user["groups"] = await _dbr_get_user_groups(conn, user_id)

        identity_id = await conn.scalar(
            identities.insert()
            .values(session_id=session["id"], user_id=user_id, auth=AuthType.PASSWORD)
            .returning(identities.c.id)
        )

    session[IDENTITY_SESSION_NAME] = identity_id
    return JSONResponse(user.to_dict(), headers=(session.get_header(),))


async def signout(scope: HTTPScope) -> HTTPApp:
    session = Session.from_headers(scope["headers"])
    if not session:
        return HTTP_400_BAD_REQUEST_RESPONSE  # TODO: which status code?

    identity_id = session.get(IDENTITY_SESSION_NAME, None)
    if not identity_id:
        return HTTP_400_BAD_REQUEST_RESPONSE  # TODO: which status code?

    async with database.begin() as conn:
        await conn.execute(
            identities.update()
            .values(disabled_at=datetime.now())
            .where(
                and_(identities.c.id == identity_id, identities.c.disabled_at.is_(None))
            )
        )

    del session[IDENTITY_SESSION_NAME]

    return RedirectResponse("/", headers=(session.get_header(),))


api_node = APINode(
    "auth",
    1,
    [
        HTTPAppMapNode(current_path_is("/signin"), POST(signin)),
        HTTPAppMapNode(current_path_is("/signout"), GET(signout)),
    ],
    {database, server},
)
