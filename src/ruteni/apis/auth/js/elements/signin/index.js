/* eslint require-jsdoc: off */
/* eslint lines-around-comment: off */
/* eslint func-style: off */
/* eslint camelcase: off */

import logging from "/static/quotquot/glue/logging.js";
import policy from "external:@totates/trusted-types/v1";
import { QuotquotElement } from "/static/quotquot/element.js";
import EventsRenderer from "/static/quotquot/events.js";
import FluentRenderer from "/static/quotquot/i18n.js";
import StylePlugin from "/static/quotquot/style.js";
import ClassRenderer from "/static/quotquot/class.js";
import html from "./index.html";

const logger = logging.getLogger("ruteni.plugin.auth.element.signin");
// logger.setLevel("WARN");

const template = document.createElement("template");
template.innerHTML = policy.createHTML(html);

const blaze = [
    "variables.host",
    "generics.global",
    "components.buttons",
    "components.inputs",
    "components.links",
    "objects.forms"
].map(name => `/static/blaze/css/${name}.css`);


class SigninFormHTMLElement extends QuotquotElement {
    constructor() {
        const state = {
            styles: blaze,
            events: {
                "submit-form": e => {
                    e.preventDefault();
                    this.submit().catch(exc => logger.error(exc));
                }
            }
        };
        super({
            template,
            state,
            Renderers: [EventsRenderer, FluentRenderer, ClassRenderer],
            Plugins: [StylePlugin]
        });
    }

    async submit() {
        // logger.debug(displayName.validity);
        // const form = this.shadowRoot.forms.signin;
        const form = this.$('form[name="signin"]');
        const { email, password } = form.elements;
        try {
            var issues = await this.call(5000, "edit", { // eslint-disable-line no-var
                email: email.value,
                password: password.value
            });
        }
        catch (exc) {
            if (exc instanceof TimeoutError)
                await this.update({ issues: { "server-timeout": true } });
            logger.error(exc);
        }
    }
}

window.customElements.define("signin-form", SigninFormHTMLElement);
