module.exports = {
    input: [
        "components/signin/index.html"
    ],
    output: ".",
    options:
    {
        debug: false,
        removeUnusedKeys: true,
        sort: true,
        attr: {
            list: ["data-i18n"],
            extensions: [".html"]
        },
        lngs: ["en", "fr"],
        ns: ["auth", "translation"],
        defaultLng: "en",
        defaultNs: "auth",
        defaultValue: "",
        resource: {
            loadPath: "i18n/{{ns}}/{{lng}}.json",
            savePath: "i18n/{{ns}}/{{lng}}.json",
            jsonIndent: 4
        },
        nsSeparator: ":",
        keySeparator: ".",
        pluralSeparator: "_",
        contextSeparator: "_",
        contextDefaultValues: [],
        interpolation: {
            prefix: "{{",
            suffix: "}}"
        }
    }
};
