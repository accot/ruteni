import { terser } from "rollup-plugin-terser";
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import html from "rollup-plugin-html";
import copy from "rollup-plugin-copy";

export default [
    {
        input: "index.js",
        output: {
            file: "../www/index.js",
            format: "esm"
        },
        external: id => id.startsWith("/static/"),
        makeAbsoluteExternalsRelative: false
    },
    ...["signin"].map(name => (
        {
            input: `elements/${name}/index.js`,
            output: {
                file: `../www/elements/${name}/index.js`,
                format: "esm"
            },
            external: id => id.startsWith("/static/"),
            makeAbsoluteExternalsRelative: false,
            plugins: [
                commonjs(),
                resolve({ browser: true }),
                html({
                    include: `elements/${name}/index.html`,
                    htmlMinifierOptions: {
                        collapseBooleanAttributes: true,
                        collapseWhitespace: true,
                        removeComments: true,
                        removeOptionalTags: true,
                        removeRedundantAttributes: true,
                        removeScriptTypeAttributes: true,
                        removeTagWhitespace: true
                    }
                }),
                copy({
                    targets: [
                        {
                            src: `elements/${name}/index.css`,
                            dest: `../www/elements/${name}`
                        }
                    ]
                }),
                process.env.BUILD === "production" && terser()
            ]
        }
    ))
];
