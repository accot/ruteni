from typing import Optional

from asgiref.typing import WebSocketCloseEvent, WebSocketScope

from ruteni.core.types import WebSocketReceive, WebSocketSend

STATUS_NORMAL = 1000


class WebSocketClose:
    def __init__(self, reason: Optional[str] = None, code: int = STATUS_NORMAL) -> None:
        self.event = WebSocketCloseEvent(
            {"type": "websocket.close", "code": code, "reason": reason}
        )

    async def __call__(
        self, scope: WebSocketScope, receive: WebSocketReceive, send: WebSocketSend
    ) -> None:
        await send(self.event)


not_a_websocket = WebSocketClose("Not Found")  # TODO: better name
