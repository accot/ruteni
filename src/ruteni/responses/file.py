import os
from email.utils import formatdate
from http import HTTPStatus
from typing import Optional, Tuple
from urllib.parse import quote

import anyio
from asgiref.typing import HTTPResponseBodyEvent, HTTPResponseStartEvent, HTTPScope

from ruteni.core.types import HTTPReceive, HTTPSend
from ruteni.routing.types import Header, PathLike
from ruteni.utils.headers import (
    CONTENT_DISPOSITION_HEADER_KEY,
    CONTENT_LENGTH_HEADER_KEY,
    CONTENT_TYPE_HEADER_KEY,
    ETAG_HEADER_KEY,
    LAST_MODIFIED_HEADER_KEY,
)


class FileResponse:
    chunk_size = 64 * 1024

    def __init__(
        self,
        path: PathLike,
        content_type: bytes,
        stat_result: os.stat_result,
        headers: Tuple[Header, ...] = (),
        status: int = HTTPStatus.OK,
        filename: Optional[str] = None,
        content_disposition_type: str = "attachment",
    ) -> None:
        self.path = path

        content_length = str(stat_result.st_size)
        last_modified = formatdate(stat_result.st_mtime, usegmt=True)
        etag = f"{stat_result.st_mtime_ns:x}-{stat_result.st_size:x}"

        all_headers = headers + (
            (CONTENT_TYPE_HEADER_KEY, content_type),
            (CONTENT_LENGTH_HEADER_KEY, content_length.encode("latin-1")),
            (LAST_MODIFIED_HEADER_KEY, last_modified.encode("latin-1")),
            (ETAG_HEADER_KEY, etag.encode("latin-1")),
        )

        if filename is not None:
            quoted_filename = quote(filename)
            content_disposition = (
                f'{content_disposition_type}; filename="{filename}"'
                if quoted_filename == filename
                else f"{content_disposition_type}; filename*=utf-8''{quoted_filename}"
            )
            all_headers += (
                (CONTENT_DISPOSITION_HEADER_KEY, content_disposition.encode("latin-1")),
            )

        self.start_event = HTTPResponseStartEvent(
            {
                "type": "http.response.start",
                "status": status,
                "headers": all_headers,
                "trailers": False,
            }
        )

    async def __call__(
        self, scope: HTTPScope, receive: HTTPReceive, send: HTTPSend
    ) -> None:
        await send(self.start_event)
        async with await anyio.open_file(self.path, mode="rb") as file:
            more_body = True
            while more_body:
                chunk = await file.read(self.chunk_size)
                more_body = len(chunk) == self.chunk_size
                body_event = HTTPResponseBodyEvent(
                    {
                        "type": "http.response.body",
                        "body": chunk,
                        "more_body": more_body,
                    }
                )
                await send(body_event)
