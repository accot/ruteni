import json
from http import HTTPStatus
from typing import Any, Tuple

from ruteni.routing.types import Header

from . import Response


class JSONResponse(Response):
    def __init__(
        self,
        content: Any,  # TODO: JSONType
        *,
        content_type: bytes = b"application/json",
        headers: Tuple[Header, ...] = (),
        status: int = HTTPStatus.OK,
    ) -> None:
        super().__init__(
            json.dumps(
                content,
                ensure_ascii=False,
                allow_nan=False,
                indent=None,
                separators=(",", ":"),
            ).encode("utf-8"),
            content_type,
            headers=headers,
            status=status,
        )
