from http import HTTPStatus
from typing import Tuple
from urllib.parse import quote

from ruteni.routing.types import Header
from ruteni.utils.headers import LOCATION_HEADER_KEY

from . import Response


class RedirectResponse(Response):
    def __init__(
        self,
        url: str,
        temporary: bool = True,
        headers: Tuple[Header, ...] = (),
    ) -> None:
        # TODO: pass |safe| parameter to |quote|
        super().__init__(
            b"",
            b"text/plain",
            headers=headers + ((LOCATION_HEADER_KEY, quote(url).encode("utf-8")),),
            status=(
                HTTPStatus.TEMPORARY_REDIRECT
                if temporary
                else HTTPStatus.PERMANENT_REDIRECT
            ),
        )
