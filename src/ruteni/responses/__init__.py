from http import HTTPStatus
from typing import Tuple

from asgiref.typing import HTTPResponseBodyEvent, HTTPResponseStartEvent, HTTPScope

from ruteni.core.types import HTTPReceive, HTTPSend
from ruteni.routing.types import Header
from ruteni.utils.headers import CONTENT_LENGTH_HEADER_KEY, CONTENT_TYPE_HEADER_KEY


class Response:
    def __init__(
        self,
        content: bytes,
        content_type: bytes,
        *,
        headers: Tuple[Header, ...] = (),
        status: int = HTTPStatus.OK,
    ) -> None:
        content_length = str(len(content)).encode("latin-1")
        self.status = status  # TODO: self.start_event["status"]?
        self.start_event = HTTPResponseStartEvent(
            {
                "type": "http.response.start",
                "status": status,
                "headers": headers
                + (
                    (CONTENT_LENGTH_HEADER_KEY, content_length),
                    (CONTENT_TYPE_HEADER_KEY, content_type),
                ),
                "trailers": False,
            }
        )
        self.body_event = HTTPResponseBodyEvent(
            {"type": "http.response.body", "body": content, "more_body": False}
        )

    async def __call__(
        self, scope: HTTPScope, receive: HTTPReceive, send: HTTPSend
    ) -> None:
        await send(self.start_event)
        await send(self.body_event)
