import logging
from collections.abc import Mapping
from typing import Iterable, Optional

from sqlalchemy import and_
from sqlalchemy.ext.asyncio import AsyncConnection
from sqlalchemy.sql import select

from ruteni.config import config
from ruteni.models.identities import identities
from ruteni.plugins.sessions import Session
from ruteni.plugins.users import User
from ruteni.routing.types import Header
from ruteni.services.database import database

logger = logging.getLogger(__name__)

IDENTITY_SESSION_NAME: str = config.get(
    "RUTENI_IDENTITY_SESSION_NAME", default="identity"
)


async def _dbr_get_user_from_identity_id(
    conn: AsyncConnection, identity_id: int
) -> Optional[User]:
    user_id = await conn.scalar(
        select(identities.c.user_id).where(
            and_(
                identities.c.id == identity_id,
                identities.c.disabled_at.is_(None),
            )
        )
    )
    assert user_id
    return await User._dbr_get_by_id(conn, user_id)


async def _dbr_get_user_from_session(
    conn: AsyncConnection, session: Session
) -> Optional[User]:
    identity_id = session.get(IDENTITY_SESSION_NAME, None)
    if not identity_id:
        return None
    return await _dbr_get_user_from_identity_id(conn, identity_id)


async def _dbr_get_user_from_headers(
    conn: AsyncConnection, headers: Iterable[Header]
) -> Optional[User]:
    session = Session.from_headers(headers)
    return await _dbr_get_user_from_session(conn, session) if session else None


async def _dbr_get_user_from_environ(
    conn: AsyncConnection, environ: Mapping[str, str]
) -> Optional[User]:
    if "HTTP_COOKIE" not in environ:
        return None
    session = Session.from_cookie(environ["HTTP_COOKIE"])
    return await _dbr_get_user_from_session(conn, session) if session else None
