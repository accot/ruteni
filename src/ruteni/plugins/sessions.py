from __future__ import annotations

import binascii
import json
import logging
from base64 import b64decode, b64encode
from collections.abc import Iterable, Iterator, Mapping
from http.cookies import Morsel, SimpleCookie
from typing import Any, Optional, Tuple

import itsdangerous
from asgiref.typing import HTTPScope
from itsdangerous.exc import BadTimeSignature, SignatureExpired

from ruteni.config import config
from ruteni.models.sessions import sessions
from ruteni.routing.types import Header
from ruteni.services.database import database
from ruteni.services.server import server
from ruteni.utils.headers import COOKIE_HEADER_KEY, find_header

logger = logging.getLogger(__name__)

SESSION_PATH: str = config.get("RUTENI_SESSION_PATH", default="/")
MAX_AGE: int = config.get("RUTENI_SESSION_MAX_AGE", cast=int, default=34560000)  # 400d
SESSION_COOKIE_NAME: str = config.get("RUTENI_SESSION_COOKIE_NAME", default="session")
SAME_SITE: str = config.get("RUTENI_SESSION_SAME_SITE", default="lax")
SECRET_KEY: str = config.get("RUTENI_SESSION_SECRET_KEY")

SESSION_ID_NAME = "id"

signer = itsdangerous.TimestampSigner(SECRET_KEY)

# TODO: sessions are now limited to 400 days; set the cookie from time to time to extend
# https://developer.chrome.com/blog/cookie-max-age-expires/


class Session(Mapping):
    def __init__(self, *args: Any, **kw: Any) -> None:
        self._storage = dict(*args, **kw)

    def __getitem__(self, key: str) -> Any:
        return self._storage[key]

    def __setitem__(self, key: str, val: Any) -> None:
        self._storage[key] = val

    def __delitem__(self, key: str) -> None:
        del self._storage[key]

    def __iter__(self) -> Iterator:
        return iter(self._storage)

    def __len__(self) -> int:
        return len(self._storage)

    def create_morsel(self) -> Morsel:
        morsel: Morsel = Morsel()
        morsel["path"] = SESSION_PATH
        if self._storage:  # false if empty
            data = json.dumps(self._storage).encode()
            value = signer.sign(b64encode(data)).decode()
            morsel.set(SESSION_COOKIE_NAME, value, value)
            # expires, comment, domain, version
            morsel["httponly"] = True
            morsel["max-age"] = MAX_AGE
            morsel["samesite"] = SAME_SITE
            morsel["secure"] = not config.is_devel
        else:
            morsel.set(SESSION_COOKIE_NAME, "", "")
            morsel["max-age"] = 0
        return morsel

    def get_header(self) -> Header:
        morsel = self.create_morsel()
        return b"Set-Cookie", morsel.OutputString().encode("latin-1")

    @classmethod
    async def create(cls, ip_address, **kwargs) -> Session:
        session = Session(**kwargs)
        async with database.begin() as conn:
            session_id = await conn.scalar(
                sessions.insert()
                .values(server_id=server.id, ip_address=ip_address)
                .returning(sessions.c.id)
            )
        session[SESSION_ID_NAME] = session_id
        return session

    @staticmethod
    def decode(signed_data: str, max_age: int = MAX_AGE) -> Optional[dict]:
        try:
            data = signer.unsign(signed_data, max_age=max_age)
        except (BadTimeSignature, SignatureExpired):
            return None
        try:
            decoded = b64decode(data)
        except binascii.Error:
            logger.warning("invalid session: %r", data)
            return None
        try:
            return json.loads(decoded)
        except json.decoder.JSONDecodeError:
            logger.warning("invalid session: %r", decoded)
            return None

    def load(self, signed_data: str, max_age: int = MAX_AGE) -> None:
        data = Session.decode(signed_data, max_age)
        if data:
            self._storage.update(data)

    @classmethod
    def from_cookie(
        cls, cookie_value: str, max_age: int = MAX_AGE
    ) -> Optional[Session]:
        cookie: SimpleCookie = SimpleCookie(cookie_value)
        morsel = cookie.get(SESSION_COOKIE_NAME, None)
        if morsel is None:
            return None
        session = cls()
        session.load(morsel.value, max_age)
        return session

    @classmethod
    def from_headers(
        cls, headers: Iterable[Header], max_age: int = MAX_AGE
    ) -> Optional[Session]:
        cookie_value = find_header(headers, COOKIE_HEADER_KEY)
        return (
            cls.from_cookie(cookie_value.decode("latin-1"), max_age)
            if cookie_value
            else None
        )

    @classmethod
    def from_environ(
        cls, environ: Mapping[str, str], max_age: int = MAX_AGE
    ) -> Optional[Session]:
        return (
            cls.from_cookie(environ["HTTP_COOKIE"], max_age)
            if "HTTP_COOKIE" in environ
            else None
        )


async def get_session_headers_provider(scope: HTTPScope) -> Tuple[Header, ...]:
    session = Session.from_headers(scope["headers"])
    return (session.get_header(),) if session else ()


async def ensure_session_headers_provider(scope: HTTPScope) -> Tuple[Header, ...]:
    session = Session.from_headers(scope["headers"])
    if not session:
        session = await Session.create(scope["client"][0] if scope["client"] else None)
    return (session.get_header(),)
