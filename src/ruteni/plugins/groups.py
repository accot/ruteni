import logging
from collections.abc import Sequence
from typing import NamedTuple, cast

from boolean import BooleanAlgebra, boolean
from sqlalchemy import and_
from sqlalchemy.ext.asyncio import AsyncConnection
from sqlalchemy.sql import select

from ruteni.models.groups import GroupStatus, groups, memberships
from ruteni.models.users import users
from ruteni.plugins.users import User
from ruteni.services.database import database

logger = logging.getLogger(__name__)


class UnknownGroupException(Exception):
    def __init__(self, group_name: str) -> None:
        super().__init__(f"unknown group: {group_name}")
        self.group_name = group_name


class InvalidExpressionException(Exception):
    def __init__(self, expression: str) -> None:
        super().__init__(f"unknown expression: {expression}")
        self.expression = expression


class MembershipInfo(NamedTuple):
    group: str
    status: GroupStatus


async def _dbr_get_user_groups(conn: AsyncConnection, user_id: int) -> Sequence[str]:
    await User._dbr_assert_exists(conn, user_id)
    return tuple(
        membership_info.group
        for membership_info in await _dbr_get_user_memberships(conn, user_id)
    )


async def dbr_get_user_groups(user_id: int) -> Sequence[str]:
    async with database.begin() as conn:
        return await _dbr_get_user_groups(conn, user_id)


async def _dbr_get_user_memberships(
    conn: AsyncConnection, user_id: int
) -> Sequence[MembershipInfo]:
    await User._dbr_assert_exists(conn, user_id)
    return tuple(
        MembershipInfo(row[0], row[1])
        for row in await conn.execute(
            select(groups.c.name, memberships.c.status)
            .select_from(memberships.join(groups).join(users))
            .where(
                and_(
                    memberships.c.user_id == user_id,
                    users.c.disabled_at.is_(None),
                    groups.c.disabled_at.is_(None),
                    memberships.c.disabled_at.is_(None),
                )
            )
        )
    )


async def dbw_add_group(name: str) -> int:
    async with database.begin() as conn:
        group_id = await conn.scalar(
            groups.insert().values(name=name).returning(groups.c.id)
        )
        return cast(int, group_id)


async def dbw_add_user_to_group(
    user_id: int, group_name: str, status: GroupStatus = GroupStatus.member
) -> int:
    async with database.begin() as conn:
        await User._dbr_assert_exists(conn, user_id)

        # TODO: try to use insert(memberships).from_select(...)
        group_id = await conn.scalar(
            select(groups.c.id).where(
                and_(groups.c.name == group_name, groups.c.disabled_at.is_(None))
            )
        )
        if group_id is None:
            raise UnknownGroupException(group_name)

        membership_id = await conn.scalar(
            memberships.insert()
            .values(user_id=user_id, group_id=group_id, status=status)
            .returning(memberships.c.id)
        )
        return cast(int, membership_id)


def eval_boolean_expression(
    expression: boolean.Expression, predicates: set[str]
) -> bool:
    if isinstance(expression, boolean.Symbol):
        logger.debug("SYM %s", expression.obj in predicates)
        return expression.obj in predicates
    elif isinstance(expression, boolean.AND):
        logger.debug("AND %s", expression.args)
        return all(eval_boolean_expression(arg, predicates) for arg in expression.args)
    elif isinstance(expression, boolean.OR):
        logger.debug("OR %s", expression.args)
        return any(eval_boolean_expression(arg, predicates) for arg in expression.args)
    elif isinstance(expression, boolean.NOT):
        logger.debug("NOT %s", expression.args)
        return not eval_boolean_expression(expression.args[0], predicates)
    elif isinstance(expression, boolean._TRUE):
        logger.debug("TRUE")
        return True
    elif isinstance(expression, boolean._FALSE):
        logger.debug("FALSE")
        return False
    else:
        raise Exception("unexpected error")


async def dbr_test_group_condition(user_id: int, condition: str) -> bool:
    async with database.connect() as conn:
        await User._dbr_assert_exists(conn, user_id)

        algebra = BooleanAlgebra()
        try:
            expression = algebra.parse(condition, simplify=False)
        except boolean.ParseError:
            raise InvalidExpressionException(condition)

        for symbol in expression.symbols:
            # TODO: test correct symbol format (zero or one colon)
            if ":" not in symbol.obj:
                symbol.obj += ":member"

        user_memberships = set(
            f"{membership_info.group}:{status.name}"
            for membership_info in await _dbr_get_user_memberships(conn, user_id)
            for status in GroupStatus
            if status.value <= membership_info.status.value
        )

    logger.debug(expression, user_memberships)
    return eval_boolean_expression(expression, user_memberships)
