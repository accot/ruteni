import logging
from collections.abc import Mapping
from typing import NamedTuple

from socketio import AsyncNamespace
from sqlalchemy import func, select

from ruteni.models.connections import connections
from ruteni.plugins.sessions import Session
from ruteni.services.database import database
from ruteni.services.server import server

logger = logging.getLogger(__name__)


class ClientInfo(NamedTuple):
    session_id: int
    connection_id: int


class BaseConnectionNamespace(AsyncNamespace):
    def __init__(self, path: str) -> None:
        super().__init__(path)
        self.clients: dict[str, ClientInfo] = {}

    async def on_connect(self, sid: str, environ: Mapping[str, str]) -> bool:
        try:
            session = Session.from_environ(environ)
            if session:
                async with database.begin() as conn:
                    # # TODO: forbid multiple connections?
                    # connection_id = await conn.scalar(
                    #     select(connections.c.id).where(
                    #         and_(
                    #             connections.c.session_id == session_id,
                    #             connections.c.closed_at.is_(None),
                    #         )
                    #     )
                    # )
                    # if connection_id is not None:
                    #     return False
                    connection_id = await conn.scalar(
                        connections.insert()
                        .values(
                            session_id=session["id"],
                            server_id=server.id,
                            sid=sid,
                            namespace=self.namespace,
                            ip_address=environ["REMOTE_ADDR"],
                        )
                        .returning(connections.c.id)
                    )
                    logger.debug("connection_id=%s", connection_id)
                    self.clients[sid] = ClientInfo(session["id"], connection_id)
                    return True
        except:
            logger.exception("connect")
        return False

    async def on_disconnect(self, sid: str) -> None:
        # TODO: is on_disconnect called if on_connect returned False?
        connection_id = self.clients[sid].connection_id
        del self.clients[sid]
        async with database.begin() as conn:
            closed_at = await conn.scalar(
                select(connections.c.closed_at).where(connections.c.id == connection_id)
            )
            assert closed_at is None
            await conn.execute(
                connections.update()
                .where(connections.c.id == connection_id)
                .values(closed_at=func.now())
            )
        logger.debug("disconnection: %s", connection_id)
