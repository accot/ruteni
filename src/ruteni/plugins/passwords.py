import logging
from typing import NamedTuple, Optional

from argon2 import PasswordHasher
from argon2.exceptions import VerifyMismatchError
from sqlalchemy import and_
from sqlalchemy.ext.asyncio import AsyncConnection
from sqlalchemy.sql import select

from ruteni.config import config
from ruteni.models.passwords import passwords
from ruteni.models.users import users
from ruteni.plugins.users import User
from ruteni.services.database import database

logger = logging.getLogger(__name__)

PASSWORD_MAX_LENGTH: int = config.get(
    "RUTENI_PASSWORD_MAX_LENGTH", cast=int, default=40
)


ph = PasswordHasher()


class Credential(NamedTuple):
    user_id: int
    hashed_password: str


async def _dbr_get_credential(
    conn: AsyncConnection, email: str
) -> Optional[Credential]:
    result = await conn.execute(
        select(users.c.id, passwords.c.hashed_password)
        .select_from(users.join(passwords))
        .where(
            and_(
                users.c.email == email,
                users.c.disabled_at.is_(None),
                passwords.c.disabled_at.is_(None),
            )
        )
    )
    row = result.first()
    return Credential(row[0], row[1]) if row else None


async def _dbr_check_password(
    conn: AsyncConnection, email: str, password: str
) -> Optional[int]:
    credential = await _dbr_get_credential(conn, email)
    if credential is None:
        return None
    logger.debug("signin: %s %s ~ %s", email, password, credential.hashed_password)
    try:
        if ph.verify(credential.hashed_password, password):
            return credential.user_id
    except VerifyMismatchError:
        pass
    return None


async def _dbw_add_password(conn: AsyncConnection, user_id: int, password: str) -> int:
    await conn.execute(
        passwords.insert()
        .values(user_id=user_id, hashed_password=ph.hash(password))
        .returning(passwords.c.id)
    )


async def dbw_register_user(
    display_name: str, email: str, locale: str, password: str
) -> tuple[int, int]:
    async with database.begin() as conn:
        user = await User._dbw_add(conn, display_name, email, locale)
        password_id = await _dbw_add_password(conn, user.id, password)
    return user.id, password_id
