from __future__ import annotations

from collections.abc import Mapping
from typing import Optional, cast

from sqlalchemy import and_
from sqlalchemy.ext.asyncio import AsyncConnection
from sqlalchemy.sql import select

from ruteni.models.locales import UnknownLocaleException, locales
from ruteni.models.users import users
from ruteni.services.database import database
from ruteni.services.locales import locales_service

# TODO: should select locales service automatically


class User:
    def __init__(
        self, user_id: int, display_name: str, email: str, locale: str
    ) -> None:
        self.id = user_id
        self.display_name = display_name
        self.email = email
        self.locale = locale

    def to_dict(self) -> dict:
        return {
            "id": self.id,
            "display_name": self.display_name,
            "email": self.email,
            "locale": self.locale,
        }

    @classmethod
    async def _dbw_add(
        cls, conn: AsyncConnection, display_name: str, email: str, locale: str
    ) -> User:
        locale_id = locales_service.get_locale_id(locale, sep="-")
        if locale_id is None:
            raise UnknownLocaleException(locale)
        user_id = await conn.scalar(
            users.insert()
            .values(display_name=display_name, email=email, locale_id=locale_id)
            .returning(users.c.id)
        )
        return cls(cast(int, user_id), display_name, email, locale)

    @classmethod
    async def dbw_add(cls, display_name: str, email: str, locale: str) -> User:
        async with database.begin() as conn:
            return await cls._dbw_add(conn, display_name, email, locale)

    @classmethod
    async def _dbr_get_by_id(
        cls, conn: AsyncConnection, user_id: int
    ) -> Optional[User]:
        result = await conn.execute(
            select(
                users.c.display_name,
                users.c.email,
                locales.c.language,
                locales.c.territory,
            )
            .select_from(users.join(locales))
            .where(and_(users.c.id == user_id, users.c.disabled_at.is_(None)))
        )
        row = result.first()
        return cls(user_id, row[0], row[1], row[2] + "-" + row[3]) if row else None

    @staticmethod
    async def dbr_get_by_email(email: str) -> Optional[User]:
        async with database.connect() as conn:
            result = await conn.execute(
                select(
                    users.c.id,
                    users.c.display_name,
                    locales.c.language,
                    locales.c.territory,
                )
                .select_from(users.join(locales))
                .where(and_(users.c.email == email, users.c.disabled_at.is_(None)))
            )
        row = result.first()
        return User(row[0], row[1], email, row[2] + "_" + row[3]) if row else None

    @classmethod
    async def _dbr_assert_exists(cls, conn: AsyncConnection, user_id: int) -> None:
        if await cls._dbr_get_by_id(conn, user_id) is None:
            raise UnknownUserException(user_id)


class UnknownUserException(Exception):
    def __init__(self, user_id: int) -> None:
        super().__init__(f"unknown user ID {user_id}")
        self.user_id = user_id
