from sqlalchemy import MetaData, event
from sqlalchemy.schema import CreateSchema

from ruteni.utils.module import load_all_submodules

SCHEMA_NAME = "ruteni"

metadata = MetaData(schema=SCHEMA_NAME)
event.listen(metadata, "before_create", CreateSchema(SCHEMA_NAME, if_not_exists=True))

load_all_submodules(__name__, __path__)
