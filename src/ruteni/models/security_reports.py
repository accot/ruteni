import enum

from sqlalchemy import (
    Column,
    DateTime,
    Enum,
    ForeignKey,
    Identity,
    Integer,
    Table,
    func,
)
from sqlalchemy_utils.types.json import JSONType

from ruteni.models.users import users

from . import metadata


class ReportType(enum.Enum):
    CSP = 1


security_reports = Table(
    "security_reports",
    metadata,
    Column("id", Integer, Identity(always=True), primary_key=True),
    Column("type", Enum(ReportType, schema="ruteni"), nullable=False),
    Column("user_id", Integer, ForeignKey(users.c.id), nullable=True, index=True),
    Column("report", JSONType, nullable=False),
    Column("timestamp", DateTime(timezone=True), nullable=False, server_default=func.now()),
)
