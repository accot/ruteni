from sqlalchemy import (
    DDL,
    Column,
    DateTime,
    ForeignKey,
    Identity,
    Index,
    Integer,
    String,
    Table,
    event,
    func,
)
from sqlalchemy_utils import EmailType

from ruteni.config import config
from ruteni.models.locales import locales

from . import metadata

ADMIN_NAME: str = config.get("RUTENI_ADMIN_NAME", default="admin")
ADMIN_EMAIL: str = config.get("RUTENI_ADMIN_EMAIL", default="admin")

users = Table(
    "users",
    metadata,
    Column("id", Integer, Identity(always=True), primary_key=True),
    Column("display_name", String(32), nullable=False),
    Column("email", EmailType, nullable=False),
    Column("locale_id", Integer, ForeignKey(locales.c.id), nullable=False, index=True),
    Column("added_at", DateTime(timezone=True), nullable=False, server_default=func.now()),
    Column("disabled_at", DateTime(timezone=True), default=None),
)

Index(None, users.c.added_at.desc())
Index(None, users.c.disabled_at.desc().nulls_first())
Index(
    "ix_users_unique_active_user_per_email",
    users.c.email,
    unique=True,
    postgresql_where=users.c.disabled_at.is_(None),
)

# TODO: SELECT id FROM ruteni.locales WHERE code='en-US'
# TODO: sql-escape name and email

event.listen(
    users,
    "after_create",
    DDL(
        "INSERT INTO ruteni.users (display_name, email, locale_id) VALUES ('%s', '%s', 1)"
        % (ADMIN_NAME, ADMIN_EMAIL)
    ),
)


# @event.listens_for(users, "after_create")
# def receive_after_create(target, conn, **kw):
#     conn.execute(
#         users.insert().values(
#             id=0, display_name=ADMIN_EMAIL, email=ADMIN_EMAIL, locale_id=1
#         )
#     )
