from sqlalchemy import Column, DateTime, Identity, Integer, Table, event, func, text
from sqlalchemy.engine.base import Connection

from . import metadata

tos = Table(
    "tos",
    metadata,
    Column("id", Integer, Identity(always=True), primary_key=True),
    Column("date", DateTime(timezone=True), nullable=False, server_default=func.now()),
)

"""
def after_create(target: Table, connection: Connection, **kwargs):  # type: ignore
    connection.execute(text("INSERT INTO %s DEFAULT VALUES" % target.name))


event.listen(tos, "after_create", after_create)
"""
