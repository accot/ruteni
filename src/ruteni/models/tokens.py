from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Identity,
    Integer,
    String,
    Table,
    and_,
    func,
)

from ruteni.models.sessions import sessions
from ruteni.models.users import users

from . import metadata

refresh_tokens = Table(
    "refresh_tokens",
    metadata,
    Column("id", Integer, Identity(always=True), primary_key=True),
    Column("user_id", Integer, ForeignKey(users.c.id), nullable=False, index=True),
    Column(
        "session_id", Integer, ForeignKey(sessions.c.id), nullable=False, index=True
    ),
    Column("token", String(64), unique=True, nullable=False),
    Column("issued_at", DateTime(timezone=True), nullable=False),
    Column("expires_at", DateTime(timezone=True), nullable=False),
    Column("revoked_at", DateTime(timezone=True), default=None),
)

access_tokens = Table(
    "access_tokens",
    metadata,
    Column("id", Integer, Identity(always=True), primary_key=True),
    Column(
        "refresh_token_id",
        Integer,
        ForeignKey(refresh_tokens.c.id),
        nullable=False,
        index=True,
    ),
    Column("issued_at", DateTime(timezone=True), nullable=False),
    Column("expires_at", DateTime(timezone=True), nullable=False),
)
