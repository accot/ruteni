from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Identity,
    Index,
    Integer,
    String,
    Table,
    func,
)

from ruteni.models.users import users

from . import metadata

passwords = Table(
    "passwords",
    metadata,
    Column("id", Integer, Identity(always=True), primary_key=True),
    Column("user_id", Integer, ForeignKey(users.c.id), nullable=False, index=True),
    Column("hashed_password", String, nullable=False),  # TODO: length as pref?
    Column("added_at", DateTime(timezone=True), nullable=False, server_default=func.now()),
    Column("disabled_at", DateTime(timezone=True), default=None),
)

Index(None, passwords.c.added_at.desc())
Index(None, passwords.c.disabled_at.desc().nulls_first())
Index(
    "ix_passwords_unique_active_password_per_user",
    passwords.c.user_id,
    unique=True,
    postgresql_where=passwords.c.disabled_at.is_(None),
)
