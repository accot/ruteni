from typing import Callable, cast

from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Identity,
    Index,
    Integer,
    String,
    Table,
    func,
)
from sqlalchemy_utils import IPAddressType

from ruteni.models.servers import servers
from ruteni.models.sessions import sessions

from . import metadata

# TODO: PARTITION BY RANGE (opened_at);
connections = Table(
    "connections",
    metadata,
    Column("id", Integer, Identity(always=True), primary_key=True),
    Column(
        "session_id", Integer, ForeignKey(sessions.c.id), nullable=False, index=True
    ),
    Column("server_id", Integer, ForeignKey(servers.c.id), nullable=False, index=True),
    Column("sid", String(28), nullable=False),
    Column("namespace", String, nullable=False, index=True),
    Column("ip_address", IPAddressType, nullable=False, index=True),
    Column(
        "opened_at",
        DateTime,
        nullable=False,
        server_default=cast(Callable[[], DateTime], func.now)(),
    ),
    Column("closed_at", DateTime(timezone=True), default=None),
)


Index(None, connections.c.opened_at.desc())
Index(None, connections.c.closed_at.desc().nulls_first())
Index(
    "ix_ruteni_connections_unique_active_sid_per_server",
    connections.c.server_id,
    connections.c.sid,
    unique=True,
    postgresql_where=connections.c.closed_at.is_(None),
)
