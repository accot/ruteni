import enum

from sqlalchemy import (
    Column,
    DateTime,
    Enum,
    ForeignKey,
    Identity,
    Integer,
    String,
    Table,
)

from ruteni.models.sessions import sessions

from . import metadata


class Level(enum.Enum):
    trace = 1
    debug = 2
    info = 3
    warn = 4
    error = 5


logs = Table(
    "logs",
    metadata,
    Column("id", Integer, Identity(always=True), primary_key=True),
    Column(
        "session_id", Integer, ForeignKey(sessions.c.id), nullable=False, index=True
    ),
    Column("level", Enum(Level, schema="ruteni"), nullable=False),
    Column("logger", String(255), nullable=False),
    Column("message", String, nullable=False),
    Column("timestamp", DateTime(timezone=True), nullable=False),
    Column("stacktrace", String),
)
