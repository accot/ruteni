import enum

from sqlalchemy import (
    Column,
    DateTime,
    Enum,
    ForeignKey,
    Identity,
    Index,
    Integer,
    Table,
    func,
)

from ruteni.models.sessions import sessions
from ruteni.models.users import users

from . import metadata

# TODO: better name? https://en.wikipedia.org/wiki/Authentication


class AuthType(enum.Enum):
    PASSWORD = 1


identities = Table(
    "identities",
    metadata,
    Column("id", Integer, Identity(always=True), primary_key=True),
    Column("user_id", Integer, ForeignKey(users.c.id), nullable=False, index=True),
    Column(
        "session_id", Integer, ForeignKey(sessions.c.id), nullable=False, index=True
    ),
    Column("auth", Enum(AuthType, schema="ruteni"), nullable=False),
    Column("added_at", DateTime(timezone=True), nullable=False, server_default=func.now()),
    Column("disabled_at", DateTime(timezone=True), default=None),
)

Index(None, identities.c.added_at.desc())
Index(None, identities.c.disabled_at.desc().nulls_first())
Index(
    "ix_identities_unique_active_identity_per_client",
    identities.c.session_id,
    unique=True,
    postgresql_where=identities.c.disabled_at.is_(None),
)
