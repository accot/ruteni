import enum
import logging

from sqlalchemy import (
    Column,
    DateTime,
    Enum,
    ForeignKey,
    Identity,
    Index,
    Integer,
    String,
    Table,
    event,
    func,
)
from sqlalchemy.schema import DDL

from ruteni.models.users import users

from . import metadata

logger = logging.getLogger(__name__)

groups = Table(
    "groups",
    metadata,
    Column("id", Integer, Identity(always=True), primary_key=True),
    # Column("owner_id", Integer, ForeignKey(users.c.id), nullable=False, index=True),
    Column("name", String(28), nullable=False),
    Column("added_at", DateTime(timezone=True), nullable=False, server_default=func.now()),
    Column("disabled_at", DateTime(timezone=True), default=None),
)

Index(None, groups.c.added_at.desc())
Index(None, groups.c.disabled_at.desc().nulls_first())
Index(
    "ix_groups_unique_active_group_per_name",
    groups.c.name,
    unique=True,
    postgresql_where=groups.c.disabled_at.is_(None),
)

event.listen(
    groups, "after_create", DDL("INSERT INTO ruteni.groups (name) VALUES ('admin')")
)


class GroupStatus(enum.Enum):
    member = 1
    manager = 2
    owner = 3


memberships = Table(
    "memberships",
    metadata,
    Column("id", Integer, Identity(always=True), primary_key=True),
    Column("user_id", Integer, ForeignKey(users.c.id), nullable=False, index=True),
    Column("group_id", Integer, ForeignKey(groups.c.id), nullable=False, index=True),
    Column("status", Enum(GroupStatus, metadata=metadata), nullable=False),
    Column("added_at", DateTime(timezone=True), nullable=False, server_default=func.now()),
    Column("disabled_at", DateTime(timezone=True), default=None),
)

Index(None, memberships.c.added_at.desc())
Index(None, memberships.c.disabled_at.desc().nulls_first())
Index(
    "ix_memberships_unique_active_user_group_pair",
    memberships.c.user_id,
    memberships.c.group_id,
    unique=True,
    postgresql_where=memberships.c.disabled_at.is_(None),
)
