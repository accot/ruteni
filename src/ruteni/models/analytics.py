from sqlalchemy import Column, DateTime, ForeignKey, Identity, Integer, Table, func
from sqlalchemy_utils.types.json import JSONType

from ruteni.models.sessions import sessions

from . import metadata

analytics = Table(
    "analytics",
    metadata,
    Column("id", Integer, Identity(always=True), primary_key=True),
    Column("timestamp", DateTime(timezone=True), nullable=False, server_default=func.now()),
    Column(
        "session_id", Integer, ForeignKey(sessions.c.id), nullable=False, index=True
    ),
    Column("metric", JSONType, nullable=False),
)
