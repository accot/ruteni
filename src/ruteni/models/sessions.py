from sqlalchemy import Column, DateTime, ForeignKey, Identity, Integer, Table, func
from sqlalchemy_utils import IPAddressType

from . import metadata
from .servers import servers

sessions = Table(
    "sessions",
    metadata,
    Column("id", Integer, Identity(always=True), primary_key=True),
    Column("server_id", Integer, ForeignKey(servers.c.id), nullable=False, index=True),
    Column("ip_address", IPAddressType, nullable=True, index=True),
    Column("created_at", DateTime(timezone=True), nullable=False, server_default=func.now()),
)
