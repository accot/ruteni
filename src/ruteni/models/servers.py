from sqlalchemy import Column, DateTime, Identity, Index, Integer, Table, func
from sqlalchemy_utils import IPAddressType

from . import metadata

servers = Table(
    "servers",
    metadata,
    Column("id", Integer, Identity(always=True), primary_key=True),
    Column("ip_address", IPAddressType, nullable=False, index=True),
    Column("started_at", DateTime(timezone=True), nullable=False, server_default=func.now()),
    Column("stopped_at", DateTime(timezone=True), default=None),
)

Index(None, servers.c.started_at.desc())
Index(None, servers.c.stopped_at.desc().nulls_first())
