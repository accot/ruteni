from sqlalchemy import Column, DateTime, ForeignKey, Identity, Integer, Table, func

from ruteni.models.users import users

from . import metadata

verifications = Table(
    "verifications",
    metadata,
    Column("id", Integer, Identity(always=True), primary_key=True),
    Column("user_id", Integer, ForeignKey(users.c.id), nullable=False, index=True),
    Column("sent_at", DateTime(timezone=True), nullable=False),
    Column("verified_at", DateTime(timezone=True), nullable=False, server_default=func.now()),
)
