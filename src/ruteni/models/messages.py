from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Identity,
    Integer,
    String,
    Table,
    func,
)
from sqlalchemy_utils.types.json import JSONType

from ruteni.models.sessions import sessions

from . import metadata

messages = Table(
    "messages",
    metadata,
    Column("id", Integer, Identity(always=True), primary_key=True),
    Column(
        "session_id", Integer, ForeignKey(sessions.c.id), nullable=False, index=True
    ),
    Column("namespace", String, nullable=False, index=True),
    Column("content", JSONType, nullable=False),
    Column(
        "timestamp",
        DateTime(timezone=True),
        index=True,
        nullable=False,
        server_default=func.now(),
    ),
)

cursors = Table(
    "cursors",
    metadata,
    Column("session_id", Integer, ForeignKey(sessions.c.id), primary_key=True),
    Column("timestamp", DateTime(timezone=True), index=True, nullable=False),
)

"""
SELECT pgq.create_queue('...');

DECLARE
    event pgq.event;
BEGIN
    FOR event IN SELECT * FROM pgq.get_batch_events(queue_name, batch_id) LOOP
       --email_data := event.data;
        PERFORM pgq.finish_event(event.ev_id);
        RETURN NEXT event;
    END LOOP;
    RETURN;
END;
"""
