import logging

from sqlalchemy import Column, DateTime, Index, SmallInteger, String, Table, event, func
from sqlalchemy.schema import DDL

from . import metadata

logger = logging.getLogger(__name__)

locales = Table(
    "locales",
    metadata,
    Column("id", SmallInteger, primary_key=True),
    Column("language", String(28), nullable=False),
    Column("territory", String(28), nullable=False),
    Column("added_at", DateTime(timezone=True), nullable=False, server_default=func.now()),
    Column("disabled_at", DateTime(timezone=True), default=None),
)

Index(None, locales.c.added_at.desc())
Index(None, locales.c.disabled_at.desc().nulls_first())
Index(
    "ix_locales_unique_active_language_territory",
    locales.c.language,
    locales.c.territory,
    unique=True,
    postgresql_where=locales.c.disabled_at.is_(None),
)

# TODO: populate table from /usr/lib/locale

event.listen(
    locales,
    "after_create",
    DDL(
        "INSERT INTO ruteni.locales (language, territory) VALUES ('en', 'US'), ('fr', 'FR')"
    ),
)

# import pycountry
# for lang in pycountry.languages:
#     if hasattr(lang, 'iso639_1_language'):
#         print(lang.iso639_1_language)
#     print(lang)


class UnknownLocaleException(Exception):
    def __init__(self, locale: str) -> None:
        super().__init__(f"unknown locale {locale}")
        self.locale = locale
