from pathlib import Path

import setuptools

# with open("README.md", "r") as fh:
#    long_description = fh.read()


def get_version() -> str:
    return "0.8.0"


setuptools.setup(
    version=get_version(),
    # packages=setuptools.find_packages(),
    # py_modules=["ruteni"],
    # package_data={
    #     "ruteni": [
    #         str(path).replace("ruteni/dist", "dist")
    #         for path in Path("ruteni/dist").glob("**/*")
    #     ]
    #     + [
    #         str(path).replace("ruteni/", "")
    #         for path in Path("ruteni").glob("**/resources/**/*")
    #     ]
    #     + [
    #         str(path).replace("ruteni/", "")
    #         for path in Path("ruteni").glob("**/templates/**/*")
    #     ]
    # },
    # test_suite="tests.build_test_suite",
)
