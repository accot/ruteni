## v0.9.0 (UNRELEASED)

### Changed
- based on @totates/{config,events,logging}

## v0.8.0 (2022-01-18)

### Added
- apps based on @totates/config v0.1.0
- non-terminal nodes can use an observable set

### Changed
- better names, types and structure (routing, apis, apps, plugins, endpoints, services)
- static error responses
- apps can use an external routes.json to define url routing
- decreased dependency on starlette
- using unittest.mock.patch.dict instead of test.support.EnvironmentVarGuard

## v0.7.0 (2022-01-02)

### Added
- new tree-based routing scheme
- services, endpoints and nodes loaded as entry points

### Changed
- fully ASGI compliant
- improvement service management
- reduced dependency on starlette
- web-app localization done in the frontend

## v0.6.0 (2021-11-06)

### Added
- based on totates

### Changed
- use fluent instead of i18next
- update to component 0.12 and its component.json
- installing all assets in project-level dist directory
- importmaps replaced by static translation
- customization of main html by script

## v0.5.1 (2021-10-28)

### Changed
- fixed python dependencies
- fixed missing static files

## v0.5.0 (2021-10-28)

### Added
- PWA support
- module main
- new plugins: quotquot, security, blaze, logging, site, etc.
- new helper modules: icons, locale, etc.
- Javascript components
- debian data package
- app-store structure
- front-end internationalization
- eslinting

### Changed
- top app is now starlette, not socketio
- working registration using web components
- using importmaps
- countless improvements

## v0.4.0 (2021-09-20)

### Added
- Fully typed
- Authentication infrastructure
- First unit tests

## v0.3.0 (2021-08-04)

### Added
- A scheduler using APScheduler's AsyncIOScheduler

## v0.2.0 (2021-06-30)

### Added
- Added add_static method

### Changed
- Renamed SQL columns

## v0.1.0 (2021-06-30)

Initial development release.
