import unittest

from ruteni.routing import get_route_params


class TestFunctionRouting(unittest.TestCase):
    def test_get_route_params(self) -> None:
        route = [("/foo", "a"), ("/bar", 1)]
        self.assertEqual(tuple(get_route_params(route)), (1, "a"))
        self.assertEqual(tuple(get_route_params(route, 0)), ())
        self.assertEqual(tuple(get_route_params(route, 1)), (1,))
        self.assertEqual(tuple(get_route_params(route, 2)), (1, "a"))
        self.assertEqual(tuple(get_route_params(route, 3)), (1, "a"))
        self.assertEqual(tuple(get_route_params([])), ())
