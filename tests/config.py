import tempfile

import sqlalchemy
from sqlalchemy_utils import create_database, drop_database

from ruteni.config import config
from ruteni.models import metadata

BASE_URL = "http://localhost:8000"
DOMAIN = "bar.fr"
USER_EMAIL = "testuser@" + DOMAIN
USER_NAME = "Test User"
USER_PASSWORD = "lWhgjgjgjHJy765"
USER_LOCALE = "fr-FR"
SITE_NAME = "Ruteni Test"

USER = {
    "id": 2,  # admin is 1
    "display_name": USER_NAME,
    "email": USER_EMAIL,
    "locale": "fr-FR",
}

config.set("RUTENI_ENV", "development")
config.set("RUTENI_SITE_NAME", SITE_NAME)
config.set("RUTENI_SITE_DOMAIN", "accot.fr")
config.set("RUTENI_VERIFICATION_FROM_ADDRESS", "Bar Baz <bar@baz.fr>")
config.set("RUTENI_SITE_ABUSE_URL", "<abuse_url>")
config.set("RUTENI_DATABASE_URL", "postgresql+asyncpg:///testdb")
config.set("RUTENI_DATABASE_INITIALIZE", True)
config.set("RUTENI_SESSION_SECRET_KEY", "secret-key")

# config.set("RUTENI_DATABASE_URL", "sqlite+aiosqlite:///:memory:")


def setup_database() -> None:
    engine = sqlalchemy.create_engine("postgresql:///testdb")
    try:
        create_database(engine.url)
    except sqlalchemy.exc.OperationalError:
        print("Could not create database. Make sure you are allowed to.5")
        exit(1)
    metadata.create_all(engine)
    return engine


def clear_database(engine) -> None:
    # for tbl in reversed(meta.sorted_tables):
    #    engine.execute(tbl.delete())
    drop_database(engine.url)
    engine.dispose()
