import os
from unittest import IsolatedAsyncioTestCase
from unittest.mock import patch

import httpx
from sqlalchemy.sql import select
from tests.config import BASE_URL, clear_database, setup_database
from tests.server import UvicornTestServer

from ruteni import status
from ruteni.apis import api_nodes
from ruteni.apis.security import (
    CSP_REPORT_CONTENT_TYPE,
    CSP_REPORT_KEY,
    ReportType,
    security_reports,
)
from ruteni.app import Ruteni
from ruteni.routing.extractors import PrefixExtractor
from ruteni.routing.nodes import ExtractorNode, IterableNode
from ruteni.routing.nodes.http import ALL_METHODS
from ruteni.services.database import database

CSP_REPORT_URI = BASE_URL + "/api/security/v1/csp-report"

CSP_REPORT = {
    "csp-report": {
        "document-uri": BASE_URL + "/app/store/",
        "referrer": "",
        "violated-directive": "script-src-elem",
        "effective-directive": "script-src-elem",
        "original-policy": "default-src 'self'; worker-src 'self'; frame-src 'none'; child-src 'none'; object-src 'none'; require-trusted-types-for 'script'; report-uri /api/logging/v1/csp-report/;",
        "disposition": "enforce",
        "blocked-uri": "inline",
        "line-number": 16,
        "source-file": BASE_URL + "/app/store/",
        "status-code": 200,
        "script-sample": "",
    }
}


class SecurityTestCase(IsolatedAsyncioTestCase):
    async def asyncSetUp(self) -> None:
        self.engine = setup_database()
        self.server = UvicornTestServer(
            Ruteni(
                ExtractorNode(PrefixExtractor("/api"), IterableNode(api_nodes)),
                services={"ruteni:security-api"},
            )
        )
        await self.server.up()

    async def asyncTearDown(self) -> None:
        await self.server.down()
        clear_database(self.engine)

    async def test_csp_report(self) -> None:
        async with httpx.AsyncClient() as client:
            for method in ALL_METHODS:
                if method != "POST":
                    response = await client.request(method, CSP_REPORT_URI)
                    self.assertEqual(
                        response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED
                    )

            response = await client.post(
                CSP_REPORT_URI,
                json=CSP_REPORT,
                headers={"Content-Type": CSP_REPORT_CONTENT_TYPE.decode()},
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.json(), True)
            async with database.connect() as conn:
                result = await conn.execute(
                    select(security_reports.c.type, security_reports.c.report).where(
                        security_reports.c.id == 1
                    )
                )
            row = result.first()
            assert row  # TODO: self.assert...
            self.assertEqual(row[0], ReportType.CSP)
            self.assertEqual(row[1], CSP_REPORT[CSP_REPORT_KEY])
