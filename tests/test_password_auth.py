from unittest import IsolatedAsyncioTestCase

import httpx
from tests.config import (
    BASE_URL,
    USER,
    USER_EMAIL,
    USER_LOCALE,
    USER_NAME,
    USER_PASSWORD,
    clear_database,
    setup_database,
)
from tests.server import UvicornTestServer

from ruteni import status
from ruteni.apis import api_nodes
from ruteni.app import Ruteni
from ruteni.plugins.passwords import dbw_register_user
from ruteni.plugins.sessions import Session
from ruteni.plugins.users import User
from ruteni.routing.extractors import PrefixExtractor
from ruteni.routing.nodes import ExtractorNode, IterableNode
from ruteni.routing.nodes.http import ALL_METHODS

SIGNIN_URL = BASE_URL + "/api/auth/v1/signin"
SIGNOUT_URL = BASE_URL + "/api/auth/v1/signout"
USER_INFO_URL = BASE_URL + "/api/user/v1/info"
REGISTER_URL = BASE_URL + "/api/sessions/v1/register"


class PasswordAuthTestCase(IsolatedAsyncioTestCase):
    async def asyncSetUp(self) -> None:
        self.engine = setup_database()
        self.server = UvicornTestServer(
            Ruteni(
                ExtractorNode(PrefixExtractor("/api"), IterableNode(api_nodes)),
                services={"ruteni:auth-api", "ruteni:user-api", "ruteni:session-api"},
            )
        )
        await self.server.up()

    async def asyncTearDown(self) -> None:
        await self.server.down()
        clear_database(self.engine)

    async def test_login(self) -> None:
        async with httpx.AsyncClient() as client:
            await dbw_register_user(USER_NAME, USER_EMAIL, USER_LOCALE, USER_PASSWORD)

            # signin with correct credentials but without a session
            response = await client.post(
                SIGNIN_URL, json={"email": USER_EMAIL, "password": USER_PASSWORD}
            )
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

            # register session
            response = await client.get(REGISTER_URL)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.json(), 1)

            # signin with wrong methods
            for method in ALL_METHODS:
                if method != "POST":
                    response = await client.request(method, SIGNIN_URL)
                    self.assertEqual(
                        response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED
                    )

            # signin with bad parameters
            response = await client.post(SIGNIN_URL, json={"email": "", "password": ""})
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

            # signin with an unknown email
            response = await client.post(
                SIGNIN_URL, json={"email": "foo@bar.fr", "password": ""}
            )
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

            # signin with a bad password
            response = await client.post(
                SIGNIN_URL, json={"email": USER_EMAIL, "password": "bad-password"}
            )
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

            # signin with correct credentials
            response = await client.post(
                SIGNIN_URL, json={"email": USER_EMAIL, "password": USER_PASSWORD}
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.json(), USER)
            self.assertEqual(
                Session.from_cookie(client.cookies), {"id": 1, "identity": 1}
            )

            # get user info when authenticated
            response = await client.get(USER_INFO_URL)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.json(), USER)

            # signout
            for method in ALL_METHODS:
                if method not in ("GET", "HEAD"):
                    response = await client.request(method, SIGNOUT_URL)
                    self.assertEqual(
                        response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED
                    )

            response = await client.get(SIGNOUT_URL)
            self.assertEqual(Session.from_cookie(client.cookies), {"id": 1})

            # get user info when not authenticated
            response = await client.get(USER_INFO_URL)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


if __name__ == "__main__":
    import unittest

    unittest.main()
