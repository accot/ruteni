import unittest

from ruteni.utils.headers import search_headers


class TestHeaderFunctions(unittest.TestCase):
    def test_search_headers(self) -> None:
        self.assertEqual(
            search_headers(((b"a", b"c"), (b"b", b"d")), (b"a", b"b")), [b"c", b"d"]
        )
        self.assertEqual(
            search_headers(((b"a", b"c"), (b"b", b"d")), (b"a", b"c")), [b"c", None]
        )
