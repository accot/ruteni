import unittest

from tests.config import (
    USER_EMAIL,
    USER_LOCALE,
    USER_NAME,
    clear_database,
    setup_database,
)

from ruteni.app import Ruteni
from ruteni.plugins.groups import (
    GroupStatus,
    InvalidExpressionException,
    UnknownGroupException,
    dbr_test_group_condition,
    dbw_add_group,
    dbw_add_user_to_group,
)
from ruteni.plugins.users import UnknownUserException, User


class GroupsTestCase(unittest.IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        self.engine = setup_database()
        self.ruteni = Ruteni(services={"ruteni:user-api"})
        # await self.enterAsyncContext(self.ruteni.lifespan.context_manager)
        await self.ruteni.lifespan.context_manager.__aenter__()

    async def asyncTearDown(self):
        await self.ruteni.lifespan.context_manager.__aexit__(None)
        clear_database(self.engine)

    async def test_dbw_add_to_group(self) -> None:
        user_info = await User.dbw_add(USER_NAME, USER_EMAIL, USER_LOCALE)
        user_id = user_info.id
        with self.assertRaises(UnknownGroupException):
            await dbw_add_user_to_group(user_id, "foo")
        await dbw_add_group("foo")
        self.assertEqual(await dbw_add_user_to_group(user_id, "foo"), 1)

    async def test_dbr_test_group_condition(self) -> None:
        with self.assertRaises(UnknownUserException):
            await dbr_test_group_condition(2, "foo")

        user_info = await User.dbw_add(USER_NAME, USER_EMAIL, USER_LOCALE)
        user_id = user_info.id

        self.assertFalse(await dbr_test_group_condition(user_id, "FALSE"))
        self.assertTrue(await dbr_test_group_condition(user_id, "TRUE"))

        with self.assertRaises(InvalidExpressionException):
            await dbr_test_group_condition(user_id, "(")

        self.assertFalse(await dbr_test_group_condition(user_id, "qux"))
        self.assertTrue(await dbr_test_group_condition(user_id, "not qux"))

        await dbw_add_group("foo")
        await dbw_add_group("bar")
        await dbw_add_group("baz")

        await dbw_add_user_to_group(user_id, "foo")
        self.assertTrue(await dbr_test_group_condition(user_id, "foo"))
        self.assertTrue(await dbr_test_group_condition(user_id, "foo:member"))
        self.assertFalse(await dbr_test_group_condition(user_id, "not foo:member"))
        self.assertFalse(await dbr_test_group_condition(user_id, "foo:manager"))
        self.assertTrue(await dbr_test_group_condition(user_id, "not foo:manager"))
        self.assertFalse(await dbr_test_group_condition(user_id, "foo:owner"))
        self.assertTrue(await dbr_test_group_condition(user_id, "not foo:owner"))
        self.assertFalse(await dbr_test_group_condition(user_id, "foo and bar"))
        self.assertTrue(await dbr_test_group_condition(user_id, "foo or bar"))
        self.assertTrue(await dbr_test_group_condition(user_id, "foo and not bar"))
        self.assertFalse(await dbr_test_group_condition(user_id, "foo and qux"))
        self.assertTrue(await dbr_test_group_condition(user_id, "foo or qux"))
        self.assertTrue(await dbr_test_group_condition(user_id, "foo and not qux"))
        self.assertFalse(await dbr_test_group_condition(user_id, "foo and bar and baz"))
        self.assertFalse(await dbr_test_group_condition(user_id, "foo and bar and qux"))

        await dbw_add_user_to_group(user_id, "bar", GroupStatus.manager)
        self.assertTrue(await dbr_test_group_condition(user_id, "foo"))
        self.assertTrue(await dbr_test_group_condition(user_id, "foo and bar"))
        self.assertTrue(await dbr_test_group_condition(user_id, "foo and bar:manager"))
        self.assertFalse(await dbr_test_group_condition(user_id, "foo and bar:owner"))
        self.assertFalse(await dbr_test_group_condition(user_id, "foo and bar and baz"))
        self.assertTrue(
            await dbr_test_group_condition(user_id, "foo and bar and not baz")
        )
        self.assertTrue(
            await dbr_test_group_condition(user_id, "foo and (qux or not baz)")
        )

        await dbw_add_user_to_group(user_id, "baz", GroupStatus.owner)
        self.assertTrue(await dbr_test_group_condition(user_id, "foo"))
        self.assertTrue(await dbr_test_group_condition(user_id, "foo and bar"))
        self.assertTrue(await dbr_test_group_condition(user_id, "foo and bar and baz"))
        self.assertTrue(await dbr_test_group_condition(user_id, "baz:manager"))
        self.assertTrue(await dbr_test_group_condition(user_id, "baz:owner"))


if __name__ == "__main__":
    unittest.main()
